# Protocol

`protocol` is a simple communication protocol used for the TCP communication between the `load_balancer`application and the `database` instances.

## Usage and Best Practices

All required message types are exposed via a builder interface, which ensures a correct construction of all supported message types:

* `Message` with a wrapped `request::Insert` request.
* `Message` with a wrapped `request::Delete` request.
* `Message` with a wrapped `request::GetByFullyQualifiedKey` request.
* `Message` with a wrapped `request::GetByPartialKey` request.
* `Message` with a wrapped `request::RegisterDatabase` request.
* `Message` with a wrapped `response::Insert` response.
* `Message` with a wrapped `response::Delete` response.
* `Message` with a wrapped `response::GetByFullyQualifiedKey` response.
* `Message` with a wrapped `response::GetByPartialKey` response.
* `Message` with a wrapped `response::RegisterDatabase` response.

A `Message` consists of metadata and the actual content which is either a `request::*` or `response::*` type. It is safe to assume that a Message can always be (de-)serialized via `serde`.  
The content is wrapped into a `String` but can be accessed via an accessor method and the provided `unwrap_<type>` methods.

### Basics

* Use the builder interface, it's your friend.
* Messages should always be used as a full object, so sending raw requests/responses should be avoided at all cost.

The `Message` acts as a generic wrapper (without actually being generic).  
This means messages can be handled in two steps:

```rust
// Step 1: Unwrap the message:
let msg: Message = serde_json::from_str(message.as_str()).unwrap_or_default()

// Step 2: Handle by type:
match msg.msg_type(){
  MessageType::RequestInsert => handle_request_insert(deserialize_insert(msg.content()),
  // ... and so on ...
  MessageType::Invalid => handle_request_invalid(),
}
```

### Message Types

#### Requests

##### Creating a Valid `Message` Message With a Wrapped `request::Insert` Request

```rust
let request_message = protocol::message::get_request_message()
         .with_sender("localhost:12345".to_string())
         .with_key("test_key".to_string())
         .with_value("test_value".to_string())
         .insert();
let request = protocol::message::request::deserialize_insert(request_message.content());
```

##### Creating a Valid `Message` Message With a Wrapped `request::Clear` Request

```rust
let request_message = protocol::message::get_request_message()
         .with_sender("localhost:12345".to_string())
         .with_key("test_key".to_string())
         .clean();
let request = protocol::message::request::deserialize_clear(request_message.content());
```

##### Creating a Valid `Message` Message With a Wrapped `request::GetByFullyQualifiedKey` Request

```rust
let request_message = protocol::message::get_request_message()
         .with_sender("localhost:12345".to_string())
         .with_key("test_key".to_string())
         .get_by_fully_qualified_key();
let request = protocol::message::request::deserialize_get_by_fully_qualified_key(request_message.content());
```

##### Creating a Valid `Message` Message with a Wrapped `request::GetByPartialKey` Request

```rust
let request_message = protocol::message::get_request_message()
         .with_sender("localhost:12345".to_string())
         .with_key("test_key".to_string())
         .get_by_partial_key();
let request = protocol::message::request::deserialize_get_by_partial_key(request_message.content());
```

##### Creating a Valid `Message` Message with a Wrapped `request::RegisterDatabase` Request

```rust
let request_message = protocol::message::get_request_message()
         .with_sender("http://testhost::12345".to_string())
         .with_host_address("http://a-test-host.com".to_string())
         .with_port("112233".to_string())
         .get_register_db();
let request = protocol::message::request::deserialize_register_database(request_message.content());
```

#### Responses

##### Creating a Valid `Message` with a Wrapped `response::Insert` Response

```rust
let response_message = protocol::message::get_response_message()
         .with_sender("localhost:12345".to_string())
         .with_status(protocol::message::response::Status::Success)
         .insert();
let response = protocol::message::response::deserialize_insert(response_message.content());
```

##### Creating a Valid `Message` with a Wrapped `response::Clear` Response

```rust
let response_message = protocol::message::get_response_message()
         .with_sender("localhost:12345".to_string())
         .with_status(protocol::message::response::Status::Success)
         .clear();
let response = protocol::message::response::deserialize_clear(response_message.content());
```

##### Creating a Valid `Message` with a Wrapped `response::GetByFullyQualifiedKey` Response

```rust
let response_message = protocol::message::get_response_message()
         .with_sender("localhost:12345".to_string())
         .with_status(protocol::message::response::Status::Success)
         .with_value("test_value".to_string())
         .get_by_fully_qualified_key();
let response = protocol::message::response::deserialize_get_by_fully_qualified_key(response_message.content());
```

##### Creating a Valid `Message` with a Wrapped `response::GetByPartialKey` Response

```rust
let response_message = protocol::message::get_response_message()
         .with_sender("localhost:12345".to_string())
         .with_status(protocol::message::response::Status::Success)
         .with_value("test_value".to_string())
         .get_by_partial_key();
let response = protocol::message::response::deserialize_get_by_partial_key(response_message.content());
```

##### Creating a Valid `Message` With a Wrapped `response::RegisterDatabase` Message Response

```rust
let response_message = protocol::message::get_response_message()
         .with_sender("localhost:12345".to_string())
         .with_status(protocol::message::response::Status::Success)
         .register_db();
let response = protocol::message::response::deserialize_register_database(response_message.content());
```
