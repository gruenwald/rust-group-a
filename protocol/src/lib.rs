//! # Protocol
//!
//! `protocol` is a simple communication protocol used for the TCP communication
//! between the `load_balancer`application and the `database` instances.

/// Contains all messages from the protocol library.
pub mod message;

#[cfg(test)]
mod tests {
    use super::*;

    mod builder {
        use super::*;

        mod request {
            use super::*;

            #[test]
            fn get_insert() {
                let req = message::get_request_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_key("test_key".to_string())
                    .with_value("test_value".to_string())
                    .insert();
                assert_eq!(message::MessageType::RequestInsert, req.msg_type());
                let request = message::request::deserialize_insert(req.content());
                assert!(request.is_valid());
                assert_eq!("test_key", request.key());
                assert_eq!("test_value", request.value());
            }

            #[test]
            fn get_clear() {
                let req = message::get_request_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_key("test_key".to_string())
                    .clear();
                assert_eq!(message::MessageType::RequestClear, req.msg_type());
                let request = message::request::deserialize_clear(req.content());
                assert!(request.is_valid());
                assert_eq!("test_key", request.key());
            }

            #[test]
            fn get_get_by_fully_qualified_key() {
                let req = message::get_request_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_key("test_key".to_string())
                    .get_by_fully_qualified_key();
                assert_eq!(
                    message::MessageType::RequestGetByFullyQualifiedKey,
                    req.msg_type()
                );
                let request =
                    message::request::deserialize_get_by_fully_qualified_key(req.content());
                assert!(request.is_valid());
                assert_eq!("test_key", request.key());
            }

            #[test]
            fn get_get_by_partial_key() {
                let req = message::get_request_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_key("test_key".to_string())
                    .get_by_partial_key();
                assert_eq!(message::MessageType::RequestGetByPartialKey, req.msg_type());
                let request = message::request::deserialize_get_by_partial_key(req.content());
                assert!(request.is_valid());
                assert_eq!("test_key", request.key());
            }

            #[test]
            fn get_register_db() {
                let req = message::get_request_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_host_address("http://a-test-host.com".to_string())
                    .with_port("112233".to_string())
                    .get_register_db();
                assert_eq!(
                    message::MessageType::RequestRegisterDatabase,
                    req.msg_type()
                );
                let request = message::request::deserialize_register_database(req.content());
                assert!(request.is_valid());
                assert_eq!(
                    "http://a-test-host.com".to_string(),
                    *request.host_address()
                );
                assert_eq!("112233", request.port());
            }
        }
        mod response {
            use super::*;

            #[test]
            fn get_insert() {
                let response = message::get_response_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_status(message::response::Status::Success)
                    .insert();
                assert_eq!(message::MessageType::ResponseInsert, response.msg_type());
                let resp = message::response::deserialize_insert(response.content());
                assert!(resp.is_valid());
                assert_eq!(message::response::Status::Success, resp.status());
            }

            #[test]
            fn get_clear() {
                let response = message::get_response_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_status(message::response::Status::Success)
                    .clear();
                assert_eq!(message::MessageType::ResponseClear, response.msg_type());
                let resp = message::response::deserialize_clear(response.content());
                assert!(resp.is_valid());
                assert_eq!(message::response::Status::Success, resp.status());
            }
            #[test]
            fn get_register_db() {
                let response = message::get_response_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_status(message::response::Status::Success)
                    .register_db();
                assert_eq!(
                    message::MessageType::ResponseRegisterDatabase,
                    response.msg_type()
                );
                let resp = message::response::deserialize_register_database(response.content());
                assert!(resp.is_valid());
                assert_eq!(message::response::Status::Success, resp.status());
            }
            #[test]
            fn get_get_by_fully_qualified_key() {
                let response = message::get_response_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_status(message::response::Status::Success)
                    .with_value("test_value".to_string())
                    .get_by_fully_qualified_key();
                assert_eq!(
                    message::MessageType::ResponseGetByFullyQualifiedKey,
                    response.msg_type()
                );
                let resp =
                    message::response::deserialize_get_by_fully_qualified_key(response.content());
                assert!(resp.is_valid());
                assert_eq!(message::response::Status::Success, resp.status());
                assert_eq!("test_value", resp.value())
            }

            #[test]
            fn get_get_by_partial_key() {
                let response = message::get_response_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_status(message::response::Status::Success)
                    .with_value("test_value".to_string())
                    .get_by_partial_key();
                assert_eq!(
                    message::MessageType::ResponseGetByPartialKey,
                    response.msg_type()
                );
                let resp = message::response::deserialize_get_by_partial_key(response.content());
                assert!(resp.is_valid());
                assert_eq!(message::response::Status::Success, resp.status());
                assert_eq!("test_value", resp.value())
            }
        }
    }

    mod serialization {
        use super::*;
        mod request {
            use super::*;

            #[test]
            fn to_json_for_insert() {
                let original_request = message::get_request_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_key("test_key".to_string())
                    .with_value("test_value".to_string())
                    .insert();

                let to_json_success = match serde_json::to_string(&original_request) {
                    Ok(_) => true,
                    _ => false,
                };
                assert!(to_json_success);
            }

            #[test]
            fn from_json_for_insert() {
                let request_message: message::Message =
                    serde_json::from_str(
                        "{\"sender\":\"http://testhost::12345\",\"msg_type\":\"RequestInsert\",\"expected_response_type\":\"ResponseInsert\",\"msg_content\":\"{\\\"valid\\\":true,\\\"key\\\":\\\"test_key\\\",\\\"value\\\":\\\"test_value\\\"}\"}")
                    .unwrap();
                assert_eq!(
                    message::MessageType::RequestInsert,
                    request_message.msg_type()
                );
                let request = message::request::deserialize_insert(request_message.content());
                assert!(request.is_valid());
                assert_eq!("test_key", request.key());
                assert_eq!("test_value", request.value());
            }
            #[test]
            fn to_json_for_clear() {
                let original_request = message::get_request_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_key("test_key".to_string())
                    .clear();

                let to_json_success = match serde_json::to_string(&original_request) {
                    Ok(_) => true,
                    _ => false,
                };
                assert!(to_json_success);
            }
            #[test]
            fn from_json_for_clear() {
                let request_message: message::Message =
                    serde_json::from_str(
                        "{\"sender\":\"http://testhost::12345\",\"msg_type\":\"RequestClear\",\"expected_response_type\":\"ResponseClear\",\"msg_content\":\"{\\\"valid\\\":true,\\\"key\\\":\\\"test_key\\\"}\"}")
                    .unwrap();
                assert_eq!(
                    message::MessageType::RequestClear,
                    request_message.msg_type()
                );
                let request = message::request::deserialize_clear(request_message.content());
                assert!(request.is_valid());
                assert_eq!("test_key", request.key());
            }

            #[test]
            fn to_json_for_get_by_fully_qualified_key() {
                let original_request = message::get_request_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_key("test_key".to_string())
                    .get_by_fully_qualified_key();

                let to_json_success = match serde_json::to_string(&original_request) {
                    Ok(_) => true,
                    _ => false,
                };
                assert!(to_json_success);
            }

            #[test]
            fn from_json_for_get_by_fully_qualified_key() {
                let request_message: message::Message =
                    serde_json::from_str(
                        "{\"sender\":\"http://testhost::12345\",\"msg_type\":\"RequestGetByFullyQualifiedKey\",\"expected_response_type\":\"ResponseGetByFullyQualifiedKey\",\"msg_content\":\"{\\\"valid\\\":true,\\\"key\\\":\\\"test_key\\\"}\"}")
                    .unwrap();
                assert_eq!(
                    message::MessageType::RequestGetByFullyQualifiedKey,
                    request_message.msg_type()
                );
                let request = message::request::deserialize_get_by_fully_qualified_key(
                    request_message.content(),
                );
                assert!(request.is_valid());
                assert_eq!("test_key", request.key());
            }

            #[test]
            fn to_json_for_get_by_partial_key() {
                let original_request = message::get_request_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_key("test_key".to_string())
                    .get_by_partial_key();

                let to_json_success = match serde_json::to_string(&original_request) {
                    Ok(_) => true,
                    _ => false,
                };
                assert!(to_json_success);
            }

            #[test]
            fn from_json_for_get_by_partial_key() {
                let request_message: message::Message =
                    serde_json::from_str(
                        "{\"sender\":\"http://testhost::12345\",\"msg_type\":\"RequestGetByPartialKey\",\"expected_response_type\":\"ResponseGetByPartialKey\",\"msg_content\":\"{\\\"valid\\\":true,\\\"key\\\":\\\"test_key\\\"}\"}")
                    .unwrap();
                assert_eq!(
                    message::MessageType::RequestGetByPartialKey,
                    request_message.msg_type()
                );
                let request =
                    message::request::deserialize_get_by_partial_key(request_message.content());
                assert!(request.is_valid());
                assert_eq!("test_key", request.key());
            }

            #[test]
            fn to_json_for_register_database() {
                let original_request = message::get_request_message()
                    .with_sender("http://testhost::12345".to_string())
                    .with_host_address("http://a-test-host.com".to_string())
                    .with_port("112233".to_string())
                    .get_register_db();

                let to_json_success = match serde_json::to_string(&original_request) {
                    Ok(_) => true,
                    _ => false,
                };
                assert!(to_json_success);
            }

            #[test]
            fn from_json_for_register_database() {
                let request_message: message::Message =
                    serde_json::from_str(
                        "{\"sender\":\"http://testhost::12345\",\"msg_type\":\"RequestRegisterDatabase\",\"expected_response_type\":\"ResponseRegisterDatabase\",\"msg_content\":\"{\\\"valid\\\":true,\\\"host_address\\\":\\\"http://a-test-host.com\\\",\\\"port\\\":\\\"12345\\\"}\"}")
                    .unwrap();
                assert_eq!(
                    message::MessageType::RequestRegisterDatabase,
                    request_message.msg_type()
                );
                let request =
                    message::request::deserialize_register_database(request_message.content());
                assert!(request.is_valid());
                assert_eq!("http://a-test-host.com", request.host_address());
                assert_eq!("12345", request.port());
            }
        }
    }
}
