use log::*;
use serde::Deserialize;
use serde::Serialize;
use std::fmt::Debug;

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Debug)]
pub enum Status {
    Success,
    Failure,
    Inconclusive,
}

impl Default for Status {
    fn default() -> Self {
        Status::Inconclusive
    }
}

/// The database insert response.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct Insert {
    /// Flag that indicates a valid Insert response.
    pub(self) valid: bool,
    /// Status of request.
    pub(self) status: Status,
}

impl Insert {
    /// Creates a new Insert response.
    /// # Arguments
    /// * `status` - Insert Status.
    pub(super) fn new(status: Status) -> Insert {
        Insert {
            valid: true,
            status,
        }
    }
    /// Returns status of insert response.
    pub fn status(&self) -> Status {
        self.status
    }
    /// Returns validity of insert response.
    pub fn is_valid(&self) -> bool {
        self.valid
    }
}

/// Deserializes a `response::Insert` from a given `&str`
/// Returns `response::Insert::default()` if serialization fails.
pub fn deserialize_insert(s: &str) -> Insert {
    let insert = serde_json::from_str(s);
    match insert {
        Ok(i) => {
            debug!("Deserializing: {:?}", i);
            i
        }
        Err(e) => {
            error!(
                "Not able to deserialize an Insert response from the provided data. {}",
                e
            );
            Insert::default()
        }
    }
}

/// The database clear response.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct Clear {
    /// Flag that indicates a valid Clear response.
    pub(self) valid: bool,
    /// Status of request.
    pub(self) status: Status,
}

impl Clear {
    /// Creates a new Clear response.
    /// # Arguments
    /// * `status` - Insert Status.
    pub(super) fn new(status: Status) -> Clear {
        Clear {
            valid: true,
            status,
        }
    }
    /// Returns Status of clear response.
    pub fn status(&self) -> Status {
        self.status
    }
    /// Retruns validity of clear response.
    pub fn is_valid(&self) -> bool {
        self.valid
    }
}

/// Deserializes a `response::Clear` from a given `&str`
/// Returns `response::Clear::default()` if serialization fails.
pub fn deserialize_clear(s: &str) -> Clear {
    let clear = serde_json::from_str(s);
    match clear {
        Ok(i) => {
            debug!("Deserializing: {:?}", i);
            i
        }
        Err(e) => {
            error!(
                "Not able to deserialize a clear response from the provided data. {}",
                e
            );
            Clear::default()
        }
    }
}

/// The database GetByFullyQualifiedKey response.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct GetByFullyQualifiedKey {
    /// Flag that indicates a valid GetByFullyQualifiedKey response.
    pub(self) valid: bool,
    /// Status of request.
    pub(self) status: Status,
    /// The value.
    pub(self) value: String,
}

impl GetByFullyQualifiedKey {
        /// Creates a new GetByFullyQualifiedKey response.
    /// # Arguments
    /// * `status` - GetByFullyQualifiedKey Status.
    pub(super) fn new(status: Status, value: String) -> GetByFullyQualifiedKey {
        GetByFullyQualifiedKey {
            valid: true,
            status,
            value,
        }
    }
    /// Returns status of GetByFullyQualifiedKey response.
    pub fn status(&self) -> Status {
        self.status
    }
    /// Value accessor.
    pub fn value(&self) -> &String {
        &self.value
    }
    /// Returns validity of GetByFullyQualifiedKey response.
    pub fn is_valid(&self) -> bool {
        self.valid
    }
}

/// Deserializes a `response::GetByFullyQualifiedKey` from a given `&str`.
/// Returns `response::GetByFullyQualifiedKey::default()` if serialization fails.
pub fn deserialize_get_by_fully_qualified_key(s: &str) -> GetByFullyQualifiedKey {
    let get_by_fully_qualified_key = serde_json::from_str(s);
    match get_by_fully_qualified_key {
        Ok(i) => {
            debug!("Deserializing: {:?}", i);
            i
        }
        Err(e) => {
            error!(
                "Not able to deserialize a GetByFullyQualifiedKey response from the provided data. {}",
                e
            );
            GetByFullyQualifiedKey::default()
        }
    }
}

/// The database GetByPartialKey response.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct GetByPartialKey {
    /// Flag that indicates a valid GetByPartialKey response.
    pub(self) valid: bool,
    /// Status of request.
    pub(self) status: Status,
    /// The value.
    pub(self) value: String,
}

impl GetByPartialKey {
    /// Creates a new GetByPartialKey response.
    /// # Arguments
    /// * `status` - GetByPartialKey status.
    /// * `value` - value.
    pub(super) fn new(status: Status, value: String) -> GetByPartialKey {
        GetByPartialKey {
            valid: true,
            status,
            value,
        }
    }
    /// Returns status of GetByPartialKey response.
    pub fn status(&self) -> Status {
        self.status
    }
    /// Value accessor.
    pub fn value(&self) -> &String {
        &self.value
    }
    /// Returns validity of GetByPartialKey response.
    pub fn is_valid(&self) -> bool {
        self.valid
    }
}


/// Deserializes a `response::GetByPartialKey` from a given `&str`.
/// Returns `response::GetByPartialKey::default()` if serialization fails.
pub fn deserialize_get_by_partial_key(s: &str) -> GetByPartialKey {
    let get_by_partial_key = serde_json::from_str(s);
    match get_by_partial_key {
        Ok(i) => {
            debug!("Deserializing: {:?}", i);
            i
        }
        Err(e) => {
            error!(
                "Not able to deserialize a GetByPartialKey response from the provided data. {}",
                e
            );
            GetByPartialKey::default()
        }
    }
}

/// The database RegisterDatabase response.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct RegisterDatabase {
    /// Flag that indicates a valid RegisterDatabase response.
    pub(self) valid: bool,
    /// Status of request.
    pub(self) status: Status,
}

impl RegisterDatabase {
        /// Creates a new RegisterDatabase response.
    /// # Arguments
    /// * `status` - RegisterDatabase status.
    pub(super) fn new(status: Status) -> RegisterDatabase {
        RegisterDatabase {
            valid: true,
            status,
        }
    }
    /// Returns status of RegisterDatabase response.
    pub fn status(&self) -> Status {
        self.status
    }
    /// Returns validity of RegisterDatabase response.
    pub fn is_valid(&self) -> bool {
        self.valid
    }
}


/// Deserializes a `response::RegisterDatabase` from a given `&str`.
/// Returns `response::RegisterDatabase::default()` if serialization fails.
pub fn deserialize_register_database(s: &str) -> RegisterDatabase {
    let register_database = serde_json::from_str(s);
    match register_database {
        Ok(i) => {
            debug!("Deserializing: {:?}", i);
            i
        }
        Err(e) => {
            error!(
                "Not able to deserialize a RegisterDatabase response from the provided data. {}",
                e
            );
            RegisterDatabase::default()
        }
    }
}
