use crate::message::request;
use crate::message::IsNotSet;
use crate::message::IsSet;
use crate::message::Message;
use crate::message::MessageType;
use crate::message::ToAssign;
use std::marker::PhantomData;

/// The Request Builder
#[derive(Debug, Clone, Default)]
pub struct Builder<SenderSet, HostSet, PortSet, KeySet, ValueSet>
where
    SenderSet: ToAssign,
    HostSet: ToAssign,
    PortSet: ToAssign,
    KeySet: ToAssign,
    ValueSet: ToAssign,
{
    /// Marker
    __sender_set: PhantomData<SenderSet>,
    /// Marker
    __host_set: PhantomData<HostSet>,
    /// Marker
    __port_set: PhantomData<PortSet>,
    /// Marker
    __key_set: PhantomData<KeySet>,
    /// Marker
    __value_set: PhantomData<ValueSet>,
    /// The message's sender (Required)
    __sender: String,
    /// The message's host (Optional for most requests)
    __host: String,
    /// The message's host's port (Optional for most requests)
    __port: String,
    /// Database key (Optional for some requests)
    __key: String,
    /// The value to store (Optional for most requests)
    __value: String,
}

impl<SenderSet, HostSet, PortSet, KeySet, ValueSet>
    Builder<SenderSet, HostSet, PortSet, KeySet, ValueSet>
where
    SenderSet: ToAssign,
    HostSet: ToAssign,
    PortSet: ToAssign,
    KeySet: ToAssign,
    ValueSet: ToAssign,
{
    /// Create a request with the given database key set.
    pub fn with_sender(self, sender: String) -> Builder<IsSet, HostSet, PortSet, KeySet, ValueSet> {
        Builder {
            __sender_set: PhantomData {},
            __host_set: PhantomData {},
            __port_set: PhantomData {},
            __key_set: PhantomData {},
            __value_set: PhantomData {},
            __sender: sender,
            __host: self.__host,
            __port: self.__port,
            __key: self.__key,
            __value: self.__value,
        }
    }
    /// Create a request with the given database key set.
    pub fn with_key(self, key: String) -> Builder<SenderSet, HostSet, PortSet, IsSet, ValueSet> {
        Builder {
            __sender_set: PhantomData {},
            __host_set: PhantomData {},
            __port_set: PhantomData {},
            __key_set: PhantomData {},
            __value_set: PhantomData {},
            __sender: self.__sender,
            __host: self.__host,
            __port: self.__port,
            __key: key,
            __value: self.__value,
        }
    }
    /// Create a request with the given database value set.
    pub fn with_value(self, value: String) -> Builder<SenderSet, HostSet, PortSet, KeySet, IsSet> {
        Builder {
            __sender_set: PhantomData {},
            __host_set: PhantomData {},
            __port_set: PhantomData {},
            __key_set: PhantomData {},
            __value_set: PhantomData {},
            __sender: self.__sender,
            __host: self.__host,
            __port: self.__port,
            __key: self.__key,
            __value: value,
        }
    }
    /// Create a request with the given host address set.
    pub fn with_host_address(
        self,
        host_address: String,
    ) -> Builder<SenderSet, IsSet, PortSet, KeySet, ValueSet> {
        Builder {
            __sender_set: PhantomData {},
            __host_set: PhantomData {},
            __port_set: PhantomData {},
            __key_set: PhantomData {},
            __value_set: PhantomData {},
            __sender: self.__sender,
            __host: host_address,
            __port: self.__port,
            __key: self.__key,
            __value: self.__value,
        }
    }
    /// Create a request with the given port set.
    pub fn with_port(self, port: String) -> Builder<SenderSet, HostSet, IsSet, KeySet, ValueSet> {
        Builder {
            __sender_set: PhantomData {},
            __host_set: PhantomData {},
            __port_set: PhantomData {},
            __key_set: PhantomData {},
            __value_set: PhantomData {},
            __sender: self.__sender,
            __host: self.__host,
            __port: port,
            __key: self.__key,
            __value: self.__value,
        }
    }
}

impl Builder<IsSet, IsNotSet, IsNotSet, IsSet, IsSet> {
    /// Creates a valid `Message` with a wrapped `request::Insert` request.
    ///
    /// # Examples
    ///
    /// ```
    /// let request_message = protocol::message::get_request_message()
    ///          .with_sender("localhost:12345".to_string())
    ///          .with_key("test_key".to_string())
    ///          .with_value("test_value".to_string())
    ///          .insert();
    /// let request = protocol::message::request::deserialize_insert(request_message.content());
    /// ```
    pub fn insert(self) -> Message {
        Message {
            sender: self.__sender,
            msg_type: MessageType::RequestInsert,
            expected_response_type: MessageType::ResponseInsert,
            msg_content: serde_json::to_string(&request::Insert::new(self.__key, self.__value))
                .unwrap_or_default(),
        }
    }
}

impl Builder<IsSet, IsSet, IsSet, IsNotSet, IsNotSet> {
    /// Creates a valid `Message` with a wrapped `request::RegisterDatabase` request.
    ///
    /// # Examples
    ///
    /// ```
    /// let request_message = protocol::message::get_request_message()
    ///          .with_sender("http://testhost::12345".to_string())
    ///          .with_host_address("http://a-test-host.com".to_string())
    ///          .with_port("112233".to_string())
    ///          .get_register_db();
    /// let request = protocol::message::request::deserialize_register_database(request_message.content());
    /// ```
    pub fn get_register_db(self) -> Message {
        Message {
            sender: self.__sender,
            msg_type: MessageType::RequestRegisterDatabase,
            expected_response_type: MessageType::ResponseRegisterDatabase,
            msg_content: serde_json::to_string(&request::RegisterDatabase::new(
                self.__host,
                self.__port,
            ))
            .unwrap_or_default(),
        }
    }
}

impl Builder<IsSet, IsNotSet, IsNotSet, IsSet, IsNotSet> {
    /// Creates a valid `Message` with a wrapped `request::Clear` request.
    ///
    /// # Examples
    ///
    /// ```
    /// let request_message = protocol::message::get_request_message()
    ///          .with_sender("localhost:12345".to_string())
    ///          .with_key("test_key".to_string())
    ///          .clear();
    /// let request = protocol::message::request::deserialize_clear(request_message.content());
    /// ```
    pub fn clear(self) -> Message {
        Message {
            sender: self.__sender,
            msg_type: MessageType::RequestClear,
            expected_response_type: MessageType::ResponseClear,
            msg_content: serde_json::to_string(&request::Clear::new(self.__key))
                .unwrap_or_default(),
        }
    }
    /// Creates a valid `Message` with a wrapped `request::GetByFullyQualifiedKey` request.
    ///
    /// # Examples
    ///
    /// ```
    /// let request_message = protocol::message::get_request_message()
    ///          .with_sender("localhost:12345".to_string())
    ///          .with_key("test_key".to_string())
    ///          .get_by_fully_qualified_key();
    /// let request = protocol::message::request::deserialize_get_by_fully_qualified_key(request_message.content());
    /// ```
    pub fn get_by_fully_qualified_key(self) -> Message {
        Message {
            sender: self.__sender,
            msg_type: MessageType::RequestGetByFullyQualifiedKey,
            expected_response_type: MessageType::ResponseGetByFullyQualifiedKey,
            msg_content: serde_json::to_string(&request::GetByFullyQualifiedKey::new(self.__key))
                .unwrap_or_default(),
        }
    }
    /// Creates a valid `Message` with a wrapped `request::GetByPartialKey` request.
    ///
    /// # Examples
    ///
    /// ```
    /// let request_message = protocol::message::get_request_message()
    ///          .with_sender("localhost:12345".to_string())
    ///          .with_key("test_key".to_string())
    ///          .get_by_partial_key();
    /// let request = protocol::message::request::deserialize_get_by_partial_key(request_message.content());
    /// ```
    pub fn get_by_partial_key(self) -> Message {
        Message {
            sender: self.__sender,
            msg_type: MessageType::RequestGetByPartialKey,
            expected_response_type: MessageType::ResponseGetByPartialKey,
            msg_content: serde_json::to_string(&request::GetByPartialKey::new(self.__key))
                .unwrap_or_default(),
        }
    }
}
