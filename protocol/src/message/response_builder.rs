use crate::message::response;
use crate::message::IsNotSet;
use crate::message::IsSet;
use crate::message::Message;
use crate::message::MessageType;
use crate::message::ToAssign;
use std::marker::PhantomData;

/// The Response Builder
#[derive(Debug, Clone, Default)]
pub struct Builder<SenderSet, StatusSet, ValueSet>
where
    SenderSet: ToAssign,
    StatusSet: ToAssign,
    ValueSet: ToAssign,
{
    /// Marker
    __sender_set: PhantomData<SenderSet>,
    /// Marker
    __status_set: PhantomData<StatusSet>,
    /// Marker
    __value_set: PhantomData<ValueSet>,
    /// The message's sender (Required)
    __sender: String,
    /// Database key (Required)
    __status: response::Status,
    /// The value to retrieve (Optional for most requests)
    __value: String,
}

impl<SenderSet, StatusSet, ValueSet> Builder<SenderSet, StatusSet, ValueSet>
where
    SenderSet: ToAssign,
    StatusSet: ToAssign,
    ValueSet: ToAssign,
{
    /// Create a request with the given database key set.
    pub fn with_sender(self, sender: String) -> Builder<IsSet, StatusSet, ValueSet> {
        Builder {
            __sender_set: PhantomData {},
            __status_set: PhantomData {},
            __value_set: PhantomData {},
            __sender: sender,
            __status: self.__status,
            __value: self.__value,
        }
    }
    pub fn with_status(self, status: response::Status) -> Builder<SenderSet, IsSet, ValueSet> {
        Builder {
            __sender_set: PhantomData {},
            __status_set: PhantomData {},
            __value_set: PhantomData {},
            __sender: self.__sender,
            __status: status,
            __value: self.__value,
        }
    }
    /// Create a response with the given database value set.
    pub fn with_value(self, value: String) -> Builder<SenderSet, StatusSet, IsSet> {
        Builder {
            __sender_set: PhantomData {},
            __status_set: PhantomData {},
            __value_set: PhantomData {},
            __sender: self.__sender,
            __status: self.__status,
            __value: value,
        }
    }
}

impl Builder<IsSet, IsSet, IsNotSet> {
    /// Creates a valid `Message` with a wrapped `response::Insert` response.
    ///
    /// # Examples
    ///
    /// ```
    /// let response_message = protocol::message::get_response_message()
    ///          .with_sender("localhost:12345".to_string())
    ///          .with_status(protocol::message::response::Status::Success)
    ///          .insert();
    /// let response = protocol::message::response::deserialize_insert(response_message.content());
    /// ```
    pub fn insert(self) -> Message {
        Message {
            sender: self.__sender,
            msg_type: MessageType::ResponseInsert,
            expected_response_type: MessageType::ResponseNone,
            msg_content: serde_json::to_string(&response::Insert::new(self.__status))
                .unwrap_or_default(),
        }
    }
    /// Creates a valid `Message` with a wrapped `response::Clear` response.
    ///
    /// # Examples
    ///
    /// ```
    /// let response_message = protocol::message::get_response_message()
    ///          .with_sender("localhost:12345".to_string())
    ///          .with_status(protocol::message::response::Status::Success)
    ///          .clear();
    /// let response = protocol::message::response::deserialize_clear(response_message.content());
    /// ```
    pub fn clear(self) -> Message {
        Message {
            sender: self.__sender,
            msg_type: MessageType::ResponseClear,
            expected_response_type: MessageType::ResponseNone,
            msg_content: serde_json::to_string(&response::Clear::new(self.__status))
                .unwrap_or_default(),
        }
    }
    /// Creates a valid `Message` with a wrapped `response::RegisterDatabase` response.
    ///
    /// # Examples
    ///
    /// ```
    /// let response_message = protocol::message::get_response_message()
    ///          .with_sender("localhost:12345".to_string())
    ///          .with_status(protocol::message::response::Status::Success)
    ///          .register_db();
    /// let response = protocol::message::response::deserialize_register_database(response_message.content());
    /// ```
    pub fn register_db(self) -> Message {
        Message {
            sender: self.__sender,
            msg_type: MessageType::ResponseRegisterDatabase,
            expected_response_type: MessageType::ResponseNone,
            msg_content: serde_json::to_string(&response::RegisterDatabase::new(self.__status))
                .unwrap_or_default(),
        }
    }
}

impl Builder<IsSet, IsSet, IsSet> {
    /// Creates a valid `Message` with a wrapped `response::GetByFullyQualifiedKey` response.
    ///
    /// # Examples
    ///
    /// ```
    /// let response_message = protocol::message::get_response_message()
    ///          .with_sender("localhost:12345".to_string())
    ///          .with_status(protocol::message::response::Status::Success)
    ///          .with_value("test_value".to_string())
    ///          .get_by_fully_qualified_key();
    /// let response = protocol::message::response::deserialize_get_by_fully_qualified_key(response_message.content());
    /// ```
    pub fn get_by_fully_qualified_key(self) -> Message {
        Message {
            sender: self.__sender,
            msg_type: MessageType::ResponseGetByFullyQualifiedKey,
            expected_response_type: MessageType::ResponseNone,
            msg_content: serde_json::to_string(&response::GetByFullyQualifiedKey::new(
                self.__status,
                self.__value,
            ))
            .unwrap_or_default(),
        }
    }
    /// Creates a valid `Message` with a wrapped `response::GetByPartialKey` response.
    ///
    /// # Examples
    ///
    /// ```
    /// let response_message = protocol::message::get_response_message()
    ///          .with_sender("localhost:12345".to_string())
    ///          .with_status(protocol::message::response::Status::Success)
    ///          .with_value("test_value".to_string())
    ///          .get_by_partial_key();
    /// let response = protocol::message::response::deserialize_get_by_partial_key(response_message.content());
    /// ```
    pub fn get_by_partial_key(self) -> Message {
        Message {
            sender: self.__sender,
            msg_type: MessageType::ResponseGetByPartialKey,
            expected_response_type: MessageType::ResponseNone,
            msg_content: serde_json::to_string(&response::GetByPartialKey::new(
                self.__status,
                self.__value,
            ))
            .unwrap_or_default(),
        }
    }
}
