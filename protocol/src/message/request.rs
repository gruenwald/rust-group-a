use log::*;
use serde::Deserialize;
use serde::Serialize;
use std::fmt::Debug;

/// The database insert request.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct Insert {
    /// Flag that indicates a valid Insert request.
    pub(self) valid: bool,
    /// Unique identifying key.
    pub(self) key: String,
    /// Arbitrary serialized value.
    pub(self) value: String,
}

impl Insert {
    /// Creates an new Insert Request.
    /// # Arguments
    /// * `key` - unique identifying key.
    /// * `value` - value.
    pub(super) fn new(key: String, value: String) -> Insert {
        Insert {
            valid: true,
            key,
            value,
        }
    }
    /// Returns a unique identifying key.
    pub fn key(&self) -> &String {
        &self.key
    }
    /// Returns a arbitrary serialized value.
    pub fn value(&self) -> &String {
        &self.value
    }
    /// Returns if insert request is valid.
    pub fn is_valid(&self) -> bool {
        self.valid
    }
}

/// Deserializes a `request::Insert` from a given `&str`
/// Returns `request::Insert::default()` if serialization fails.
pub fn deserialize_insert(s: &str) -> Insert {
    let insert = serde_json::from_str(s);
    match insert {
        Ok(i) => {
            debug!("Deserializing: {:?}", i);
            i
        }
        Err(e) => {
            error!(
                "Not able to deserialize an Insert request from the provided data. {}",
                e
            );
            Insert::default()
        }
    }
}

/// The database clear request.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct Clear {
    /// Flag that indicates a valid Clear request.
    pub(self) valid: bool,
    /// Unique identifying key.
    pub(self) key: String,
}

impl Clear {
    /// Creates a new clear request.
    /// # Arguments
    /// * `key` - unique identifying key.
    pub(super) fn new(key: String) -> Clear {
        Clear { valid: true, key }
    }
    /// Returns if clear request is valid.
    pub fn is_valid(&self) -> bool {
        self.valid
    }
    /// Returns a unique identifying key.
    pub fn key(&self) -> &String {
        &self.key
    }
}

/// Deserializes a `request::Clear` from a given `&str`
/// Returns `request::Clear::default()` if serialization fails.
/// # Arguments
/// * `s` - string to deserialize.
pub fn deserialize_clear(s: &str) -> Clear {
    let clear = serde_json::from_str(s);
    match clear {
        Ok(i) => {
            debug!("Deserializing: {:?}", i);
            i
        }
        Err(e) => {
            error!(
                "Not able to deserialize a clear request from the provided data. {}",
                e
            );
            Clear::default()
        }
    }
}

/// The GetByFullyQualifiedKey request.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct GetByFullyQualifiedKey {
    /// Flag that indicates a valid GetByFullyQualifiedKey request.
    pub(self) valid: bool,
    /// Unique identifying key.
    pub(self) key: String,
}

impl GetByFullyQualifiedKey {
    /// Creates a new GetByFullyQualifiedKey request.
    /// # Arguments
    /// * `key` - unique identifying key.
    pub(super) fn new(key: String) -> GetByFullyQualifiedKey {
        GetByFullyQualifiedKey { valid: true, key }
    }
    /// Returns if GetByFullyQualifiedKey request is valid.
    pub fn is_valid(&self) -> bool {
        self.valid
    }
    /// Key accessor.
    pub fn key(&self) -> &String {
        &self.key
    }
}

/// Deserializes a `request::GetByFullyQualifiedKey` from a given `&str`.
/// Returns `request::GetByFullyQualifiedKey::default()` if serialization fails.
pub fn deserialize_get_by_fully_qualified_key(s: &str) -> GetByFullyQualifiedKey {
    let get_by_fully_qualified_key = serde_json::from_str(s);
    match get_by_fully_qualified_key {
        Ok(i) => {
            debug!("Deserializing: {:?}", i);
            i
        }
        Err(e) => {
            error!(
                "Not able to deserialize a GetByFullyQualifiedKey request from the provided data. {}",
                e
            );
            GetByFullyQualifiedKey::default()
        }
    }
}

/// The database GetByPartialKey. Exectures a broad search.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct GetByPartialKey {
    /// Flag that indicates a valid GetByPartialKey request.
    pub(self) valid: bool,
    /// Unique identifying key.
    pub(self) key: String,
}

impl GetByPartialKey {
    /// Creates a new GetByPartialKey request.
    /// # Arguments
    /// * `key` - unique identifying key.
    pub(super) fn new(key: String) -> GetByPartialKey {
        GetByPartialKey { valid: true, key }
    }
    /// Returns if GetByPartialKey request is valid.
    pub fn is_valid(&self) -> bool {
        self.valid
    }
    /// Key accessor.
    pub fn key(&self) -> &String {
        &self.key
    }
}

/// Deserializes a `request::GetByPartialKey` from a given `&str`.
/// Returns `request::GetByPartialKey::default()` if serialization fails.
pub fn deserialize_get_by_partial_key(s: &str) -> GetByPartialKey {
    let get_by_partial_key = serde_json::from_str(s);
    match get_by_partial_key {
        Ok(i) => {
            debug!("Deserializing: {:?}", i);
            i
        }
        Err(e) => {
            error!(
                "Not able to deserialize a GetByPartialKey request from the provided data. {}",
                e
            );
            GetByPartialKey::default()
        }
    }
}

/// The `RegisterDatabase` request.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct RegisterDatabase {
    /// Flag that indicates a valid RegisterDatabase request.
    pub(self) valid: bool,
    /// Address of db.
    pub(self) host_address: String,
    /// Port of db.
    pub(self) port: String,
}

impl RegisterDatabase {
    /// Creates a new RegisterDatabase request.
    /// # Arguments
    /// * `host address` - Address of db.
    /// * `port` - Port of db.
    pub(super) fn new(host_address: String, port: String) -> RegisterDatabase {
        RegisterDatabase {
            valid: true,
            host_address,
            port,
        }
    }
    /// Returns if RegisterDatabase request is valid.
    pub fn is_valid(&self) -> bool {
        self.valid
    }
    /// Host address accessor
    pub fn host_address(&self) -> &String {
        &self.host_address
    }
    /// port address accessor
    pub fn port(&self) -> &String {
        &self.port
    }
}

/// Deserializes a `request::RegisterDatabase` from a given `&str`.
/// Returns `request::RegisterDatabase::default()` if serialization fails.
pub fn deserialize_register_database(s: &str) -> RegisterDatabase {
    let register_database = serde_json::from_str(s);
    match register_database {
        Ok(i) => {
            debug!("Deserializing: {:?}", i);
            i
        }
        Err(e) => {
            error!(
                "Not able to deserialize a RegisterDatabase request from the provided data. {}",
                e
            );
            RegisterDatabase::default()
        }
    }
}
