use serde::Deserialize;
use serde::Serialize;
use std::fmt::{Debug, Display, Formatter, Result};

/// Represents a request to a database.
pub mod request;
/// Represents a response from a database.
pub mod response;

/// Builder to put together a valid request.
pub mod request_builder;
/// Builder to put together a valid response.
pub mod response_builder;

/// Marker
#[derive(Debug, Default)]
pub struct IsSet;
/// Marker
#[derive(Debug, Default)]
pub struct IsNotSet;
/// Marker Trait
pub trait ToAssign: Debug {}
/// Marker Trait
pub trait Assigned: ToAssign {}
/// Marker Trait
pub trait NotAssigned: ToAssign {}

impl ToAssign for IsSet {}
impl ToAssign for IsNotSet {}
impl Assigned for IsSet {}
impl NotAssigned for IsNotSet {}

/// Creates various types of messages:
/// * `Message` with a wrapped `request::Insert`
/// * `Message` with a wrapped `request::Clear`
/// * `Message` with a wrapped `request::GetByFullyQualifiedKey`
/// * `Message` with a wrapped `request::GetByPartialKey`
pub fn get_request_message(
) -> request_builder::Builder<IsNotSet, IsNotSet, IsNotSet, IsNotSet, IsNotSet> {
    request_builder::Builder::default()
}
/// Creates various types of messages:
/// * `Message` with a wrapped `response::Insert`
/// * `Message` with a wrapped `response::Clear`
/// * `Message` with a wrapped `response::GetByFullyQualifiedKey`
/// * `Message` with a wrapped `response::GetByPartialKey`
pub fn get_response_message() -> response_builder::Builder<IsNotSet, IsNotSet, IsNotSet> {
    response_builder::Builder::default()
}

/// Possible request types.
#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Debug)]
pub enum MessageType {
    RequestInsert,
    RequestClear,
    RequestGetByFullyQualifiedKey,
    RequestGetByPartialKey,
    RequestRegisterDatabase,
    ResponseInsert,
    ResponseClear,
    ResponseGetByFullyQualifiedKey,
    ResponseGetByPartialKey,
    ResponseRegisterDatabase,
    ResponseNone,
    /// Default Message Type for all `Message::default()` calls.
    Invalid,
}

impl Default for MessageType {
    fn default() -> Self {
        MessageType::Invalid
    }
}

impl Display for MessageType {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{:?}", self)
    }
}

/// Generic TCP request.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct Message {
    /// The message's sender: Usually `hostname:port` or similar.
    pub(self) sender: String,
    /// The message type 
    pub(self) msg_type: MessageType,
    /// The expected response type. 
    pub(self) expected_response_type: MessageType,
    /// The message content as string.
    pub(self) msg_content: String,
}

impl Message {
    pub fn msg_type(&self) -> MessageType {
        self.msg_type
    }
    pub fn expected_response_type(&self) -> MessageType {
        self.expected_response_type
    }
    pub fn sender(&self) -> &String {
        &self.sender
    }
    pub fn content(&self) -> &String {
        &self.msg_content
    }
}
