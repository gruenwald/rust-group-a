use super::sensor;

use chrono::{DateTime, Timelike, Utc};
use rand::Rng;
use std::f64::consts::E;
use weather::weather_rest_api as api;
use weather_api as weather;

///
/// The Dummy Sensor is a virtual sensor that creates artificial measurements.
///
/// Measurement range: <br>
/// Temperature:  4.6 - 28.0 degree Celsius <br>
/// Humidity: 20-60%
#[derive(Debug)]
pub struct DummySensor {
    /// Id of the Dummy Sensor.
    id: String,
    /// Name of the Dummy Sensor.
    name: String,
    /// Place the Dummy Sensor is located at.
    location: String,
    /// The type of the Dummy Sensor. Indicates what it is measuring.
    sensor_type: api::SensorType,
}

impl DummySensor {
    /// creates a new Dummy Sensor
    #[allow(dead_code)]
    pub fn new(
        a_id: String,
        a_name: String,
        location: String,
        sensor_type: api::SensorType,
    ) -> Self {
        DummySensor {
            id: a_id,
            name: a_name,
            location,
            sensor_type,
        }
    }

    ///
    /// Genrates a random measurement for the set sensortype.
    ///
    /// Measurement range: <br>
    /// Temperature:  4.6 - 28.0 degree Celsius <br>
    /// Humidity: 20-60%
    ///
    /// # Arguments 
    /// * `time` - Datetime which should be put as creation time for the masurement. Can lay in the past or future.
    fn genmeasure(&self, time: DateTime<Utc>) -> api::WeatherDataMessage {
        let mut rng = rand::thread_rng();

        match self.sensor_type {
            api::SensorType::Humidity => {
                let measurement: i32 = rng.gen_range(20i32, 60i32);
                api::WeatherDataMessage {
                    sensor_type: api::SensorType::Humidity,
                    location: self.location.clone(),
                    temperature: None,
                    temperature_unit: None,
                    humidity: Some(measurement as i32),
                    date_time: time,
                }
            }
            api::SensorType::Temperature => api::WeatherDataMessage {
                sensor_type: api::SensorType::Temperature,
                location: self.location.clone(),
                temperature: Some(rand_temp(time.hour() as f64)),
                temperature_unit: Some(api::TemperatureUnit::Celsius),
                humidity: None,
                date_time: time,
            },
        }
    }

    ///
    /// Genrates a random measurement for the set sensortype.
    ///
    /// Measurement range: <br>
    /// Temperature:  4.6 - 28.0 degree Celsius <br>
    /// Humidity: 20-60%
    ///
    /// Even though this method returns a result. Due to the type of this sensor it will always return Ok.
    ///
    /// # Arguments 
    /// * `time` - Datetime which should be put as creation time for the masurement. Can lay in the past or future.
    #[allow(dead_code)]
    pub fn take_measurement_at(
        &self,
        time: DateTime<Utc>,
    ) -> Result<api::WeatherDataMessage, Box<dyn std::error::Error>> {
        Ok(self.genmeasure(time))
    }

    ///
    /// Genrates a random measurement for the set sensortype.
    ///
    /// Measurement range: <br>
    /// Temperature:  4.6 - 28.0 degree Celsius <br>
    /// Humidity: 20-60%
    /// 
    /// # Arguments 
    /// * `time` - Datetime which should be put as creation time for the masurement. Can lay in the past or future.
    pub fn take_measurement_at_safe(&self, time: DateTime<Utc>) -> api::WeatherDataMessage {
        self.genmeasure(time)
    }

    ///
    /// Genrates a random measurement for the set sensortype.
    ///
    /// Measurement range: <br>
    /// Temperature:  4.6 - 28.0 degree Celsius <br>
    /// Humidity: 20-60%
    #[allow(dead_code)]
    pub fn take_measurement_safe(&self) -> api::WeatherDataMessage {
        self.genmeasure(Utc::now())
    }
}

impl sensor::Sensor for DummySensor {
    /// Returns the id of the Dummy Sensor.
    fn id(&self) -> &str {
        &self.id
    }
    /// Returns the name of the Dummy Sensor.
    fn name(&self) -> &str {
        &self.name
    }
    /// Returns the type of the Dummy Sensor. Indicates what it is measuring.
    fn sensortype(&self) -> api::SensorType {
        self.sensor_type
    }
    /// Returns the place the Dummy Sensor is located at.
    fn location(&self) -> &str {
        &self.location
    }

    ///
    /// Genrates a random measurement for the set sensortype.
    ///
    /// Measurement range: <br>
    /// Temperature:  4.6 - 28.0 degree Celsius <br>
    /// Humidity: 20-60%
    ///
    /// Even though this method returns a result. Due to the type of this sensor it will always return Ok.
    ///
    fn take_measurement(&self) -> Result<api::WeatherDataMessage, Box<dyn std::error::Error>> {
        Ok(self.genmeasure(Utc::now()))
    }
}

/// Sigmoid function, used by rand_temp() to generate a random temperature
fn sig(x: f64) -> f64 {
    let t = E.powf(-x);
    1.0 / (1.0 + t)
}

/// Derivation of sigmoid function, used by rand_temp() to generate a random temperature
fn dx_sig(x: f64) -> f64 {
    sig(x) * (1.0 - sig(x))
}

/// Creates a pseudo random temperature measurement
/// During noon there will be the highest temperatures.
/// At night the lowest.
///
/// Range  4.6 - 28.0 degree Celsius for valid x
///
/// # Arguments
/// * `x` - hour of the day (0 - 23)
fn rand_temp(x: f64) -> i32 {
    let mut rng = rand::thread_rng();
    let variation: f64 = rng.gen_range(-3, 3) as f64;
    (100.0 * dx_sig((x - 12.0) * 0.2) + variation) as i32
}
