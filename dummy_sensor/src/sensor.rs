use weather::weather_rest_api as api;
use weather_api as weather;

///
/// Base trait for all weather sensors, that can create weather measurements.
///
pub trait Sensor {
    /// Returns the id of the Sensor.
    fn id(&self) -> &str;
    /// Returns the name of the Sensor.
    fn name(&self) -> &str;
    /// Returns the place the Sensor is located at.
    fn location(&self) -> &str;
    /// Returns the type of the Sensor. Indicates what it is measuring.
    fn sensortype(&self) -> api::SensorType;
    /// Takes a measurement.
    fn take_measurement(&self) -> Result<api::WeatherDataMessage, Box<dyn std::error::Error>>;
}
