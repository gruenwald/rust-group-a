/// Special serializers used by the configuration parser
mod configuration_serializers;
/// Dummy Sensor Configuration
pub mod dummy_sensor_configuration;
/// Dummy Sensor Configuration Errors
pub mod dummy_sensor_error;
/// Threads used by the Dummy Sensor Application e.g. continuous mode
pub mod sensor_threads;

use super::dummy_sensor;
