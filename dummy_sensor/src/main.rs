/// Main functionality of dummy sensor application
mod app;
/// Dummy Sensor
mod dummy_sensor;
/// Base sensor definitions
mod sensor;

use app::dummy_sensor_configuration::{DummySensorConfiguration, SensorRunMode};
use app::dummy_sensor_error::{
    DummySensorConfigurationParseError, DummySensorConfigurationParseErrorKind,
};
use app::sensor_threads::*;
use sensor::Sensor;

use weather::weather_rest_api as api;
use weather_api as weather;
use weather_api::async_weather_provider as provider;

use chrono::prelude::*;
use chrono::{Duration, Utc};
use log::*;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::mpsc;
use std::sync::Arc;
use std::thread;
use tokio;
use tokio::time::timeout;

/// Version of dummy sensor application
const VERSION: &str = env!("CARGO_PKG_VERSION");

/// The main loop of the application.
/// Reads the config and distributes the work accordingly.
#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // # create config
    let c_result = DummySensorConfiguration::from_cli_arguments_or_config(VERSION, true);
    let config: DummySensorConfiguration;
    match c_result {
        Ok(c) => config = c,
        Err(DummySensorConfigurationParseError {
            kind: DummySensorConfigurationParseErrorKind::CLIHelpOrVersionDisplayed,
            message: e,
            ..
        }) => {
            println!("{}", e);
            return Ok(());
        }

        Err(e) => {
            println!("An error occured while creating the configuration");
            println!("Error: {}", e);
            println!("Aborting due to errors. Program will be closed.");
            return Ok(());
        }
    }
    // println!("{:?}", config);
    match stderrlog::new()
        .module(module_path!())
        .quiet(false)
        .verbosity(config.log_level)
        .timestamp(stderrlog::Timestamp::Millisecond)
        .init()
    {
        Ok(_) => {}
        Err(e) => {
            println!("An error occured while creating the logger");
            println!("Error: {}", e);
            println!("Aborting due to errors. Program will be closed.");
            return Ok(());
        }
    }
    info!("### Starting Dummy Weather Sensor - v{} #####", VERSION);
    info!("### Sending to {}:{}", config.lb_url, config.lb_port);

    match config.mode {
        SensorRunMode::Once => {
            info!("### Mode - ONCE");
            execute_mode_once(&config).await;
        }
        SensorRunMode::Continuous => {
            info!("### Mode - CONTINUOUS");
            info!("### you can stop this mode by entering 'q' or 'quit'");
            thread::sleep(std::time::Duration::from_secs(2)); // wait one second to give user a chance to read this message

            execute_mode_continuous(&config).await;
        }
        SensorRunMode::Batch => {
            info!("### Mode - Batch");
            execute_mode_batch(&config).await;
        }
        SensorRunMode::Demo => {
            info!("### Mode - DEMO");
            execute_mode_demo(&config).await;
        }
        SensorRunMode::None => {
            warn!("No Execution mode set!");
            info!("Doing nothing");
        }
    }

    info!("### Closing Dummy Sensor ######################");
    Ok(())
}

/// Create and send one measurement to the load balancer
/// 
/// # Arguments
/// * `config` - Dummy Sensor configuration.
/// 
async fn execute_mode_once(config: &DummySensorConfiguration) {
    let weatherprovider: provider::AsyncWeatherProvider =
        provider::AsyncWeatherProvider::new(&config.lb_url, config.lb_port);

    let my_dummy_sensor: dummy_sensor::DummySensor = dummy_sensor::DummySensor::new(
        "1".to_string(),
        config.sensorname.clone(),
        config.location.clone(),
        config.sensortype,
    );
    let startdate = config.start_datetime_utc();
    let random_measurement = my_dummy_sensor.take_measurement_at_safe(startdate);
    let current_time_str = startdate.format("%F %T");
    info!("sending {}", current_time_str);
    trace!("{:?}", random_measurement);
    match weatherprovider
        .add_weather_data_async(&random_measurement, true)
        .await
    {
        Ok(r) => match r.status().as_u16() {
            200..=299 => info!("{}", r.status()),
            _ => error!("{}", r.status()),
        },
        Err(e) => error!("An error occured during message transmission: {}", e),
    };
}
/// Create and send multiple measurements to the load balancer
/// 
/// # Arguments
/// * `config` - Dummy Sensor configuration.
/// 
async fn execute_mode_batch(config: &DummySensorConfiguration) {
    let weatherprovider: provider::AsyncWeatherProvider =
        provider::AsyncWeatherProvider::new(&config.lb_url, config.lb_port);
    let my_dummy_sensor: dummy_sensor::DummySensor = dummy_sensor::DummySensor::new(
        "Batch".to_string(),
        config.sensorname.clone(),
        config.location.clone(),
        config.sensortype,
    );
    let mut starttime = config.start_datetime_utc();
    let offset = Duration::hours(1);

    for x in 0..config.batch_mode_settings.count {
        let current_time_str = starttime.format("%F %T");
        info!(
            "sending {}/{} - {}",
            x + 1,
            config.batch_mode_settings.count,
            current_time_str
        );
        let random_measurement = my_dummy_sensor.take_measurement_at_safe(starttime);

        trace!("{:?}", random_measurement);
        let latest = x == config.batch_mode_settings.count;
        match weatherprovider
            .add_weather_data_async(&random_measurement, latest)
            .await
        {
            Ok(r) => match r.status().as_u16() {
                200..=299 => trace!("{}", r.status()),
                300..=399 => warn!("{}", r.status()),
                _ => error!("{}", r.status()), // mark every other resonse type
            },
            Err(e) => {
                error!("An error occured during message transmission: {}", e);
                error!("Aborting execution due to errors");
                break;
            }
        };

        starttime = starttime + offset;
    }
}
/// Generate temperature measurements for all locations. Ignores all arguments except of port, url and startdate.
/// 
/// # Arguments
/// * `config` - Dummy Sensor configuration.
/// 
async fn execute_mode_demo(config: &DummySensorConfiguration) {
    let weatherprovider: provider::AsyncWeatherProvider =
        provider::AsyncWeatherProvider::new(&config.lb_url, config.lb_port);

    let iter = weather::LOCATIONS.iter();
    let mut i: u32 = 0;
    let day: Date<Utc> = config.start_datetime_utc().date();
    let mut sent_error_occurred: bool = false;
    for loc in iter {
        let my_dummy_sensor: dummy_sensor::DummySensor = dummy_sensor::DummySensor::new(
            i.to_string(),
            (*loc).to_string(),
            (*loc).to_string(),
            api::SensorType::Temperature,
        );
        info!("# Starting Sensor {}", my_dummy_sensor.name());
        let mut h: u32 = 0;
        while h < 24 {
            let hour = day.and_hms(h, 0, 0);
            let current_time_str = hour.format("%F %T");
            info!("taking measurement - hour {}/23 - {}", h, current_time_str);
            let random_measurement = my_dummy_sensor.take_measurement_at_safe(hour);
            trace!("{:?}", random_measurement);
            let latest = h == 23;

            match weatherprovider
                .add_weather_data_async(&random_measurement, latest)
                .await
            {
                Ok(r) => match r.status().as_u16() {
                    200..=299 => trace!("{}", r.status()),
                    300..=399 => warn!("{}", r.status()),
                    _ => error!("{}", r.status()),
                },
                Err(e) => {
                    error!("An error occured during message transmission: {}", e);
                    error!("Aborting execution due to errors");
                    sent_error_occurred = true;
                    break;
                }
            }
            h += 1;
        }
        info!("# Stopping Sensor {}", my_dummy_sensor.name());
        if sent_error_occurred {
            break;
        }
        i += 1;
    }
}
/// Sends continuously, in a defined interval, measurements. Timestamps will increase by 1 hour each time.
/// 
/// # Arguments
/// * `config` - Dummy Sensor configuration.
/// 
async fn execute_mode_continuous(config: &DummySensorConfiguration) {
    let (tx, rx) = mpsc::channel();
    let tx_console_handler = mpsc::Sender::clone(&tx);
    let abort_sensor = Arc::new(AtomicBool::new(false));
    let continuous_sensor_aborted = abort_sensor.clone();
    let cont_sensor_config = config.clone();
    let _tid_console = 0;
    let _tid_sensor = 1;

    let _console_thread: tokio::task::JoinHandle<_> =
        tokio::task::spawn(console_command_handler(tx_console_handler, _tid_console));
    let sensor_thread = tokio::spawn(continuous_sensor_handler(
        tx,
        _tid_sensor,
        continuous_sensor_aborted,
        cont_sensor_config,
    ));

    for received in rx {
        match received {
            ThreadCallbackMessage {
                message_type: ThreadCallbackMessageKind::Quit,
                ..
            } => {
                info!("# Quitting Continuous sensor");
                abort_sensor.store(true, Ordering::SeqCst);
                break; // stop listening
            }
            ThreadCallbackMessage {
                message_type: ThreadCallbackMessageKind::Error,
                message: m,
                ..
            } => {
                error!("{}", m);
            }
            ThreadCallbackMessage {
                message_type: ThreadCallbackMessageKind::Info,
                message: m,
                ..
            } => {
                info!("{}", m);
            }
            ThreadCallbackMessage {
                message_type: ThreadCallbackMessageKind::Warn,
                message: m,
                ..
            } => {
                warn!("{}", m);
            }
            ThreadCallbackMessage {
                message_type: ThreadCallbackMessageKind::Trace,
                message: m,
                ..
            } => {
                trace!("{}", m);
            }
        }
    }

    // info!("about to exit");
    let wait_time = 5;
    drop(_console_thread);
    if timeout(std::time::Duration::from_secs(wait_time), sensor_thread)
        .await
        .is_err()
    {
        warn!(
            "Sensor thread did not exit within {}s. It may be still sleeping. Closing now anyway.",
            wait_time
        );
    }
}
