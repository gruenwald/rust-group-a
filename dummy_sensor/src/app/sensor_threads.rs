use chrono::prelude::*;
use chrono::{Duration, Utc};

use super::dummy_sensor;
use super::dummy_sensor_configuration::DummySensorConfiguration;

use weather_api::async_weather_provider as provider;

use async_std::io;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread;

/// Defines possible message types for an ThreadCallbackMessage.
#[derive(Debug)]
#[allow(dead_code)]
pub enum ThreadCallbackMessageKind {
    /// Quit Thread or Application. Actual meaning depends on context.
    Quit,
    /// Error message.
    Error,
    /// Information Message.
    Info,
    /// Warning Message.
    Warn,
    /// Trace Message.
    Trace,
}

/// Defines a message that can be used to transfer messages between threads
#[derive(Debug)]
pub struct ThreadCallbackMessage {
    /// Id of message sender.
    pub sender_id: u32,
    /// Kind of message.
    pub message_type: ThreadCallbackMessageKind,
    /// The message.
    pub message: String,
}

///
/// Thread that will continuously send Measurements to a server.
/// It will send a message and wait for a defined ammount of seconds.
///
/// Each timestamp will be increased by 1 hour.
/// Interval between measurements and start timestamp have to be configured in the configuration under the continuous_mode_settings
///
    /// # Arguments
    /// * `tx` - Channel over which notifications should be send.
    /// * `thread_id` - Thread id.
    /// * `continuous_sensor_aborted` - Bool. If set to false, Thread will stop at next check,
    /// * `config` - Dummy Sensor configuration,
pub async fn continuous_sensor_handler(
    tx: std::sync::mpsc::Sender<ThreadCallbackMessage>,
    thread_id: u32,
    continuous_sensor_aborted: Arc<AtomicBool>,
    config: DummySensorConfiguration,
) {
    /// Will try to send an log message to the listener, will log to console if noone is listening anymore
    fn try_send_log(
        sender_pipe: &std::sync::mpsc::Sender<ThreadCallbackMessage>,
        sender_id: u32,
        msg_type: ThreadCallbackMessageKind,
        msg: String,
    ) {
        let to_send = ThreadCallbackMessage {
            sender_id,
            message_type: msg_type,
            message: msg,
        };
        match sender_pipe.send(to_send) {
            Ok(()) => {}
            Err(e) => {
                println!("An error occured while notifying caller thread: {}", e);
            }
        };
    }

    let weatherprovider: provider::AsyncWeatherProvider =
        provider::AsyncWeatherProvider::new(&config.lb_url, config.lb_port);
    let my_dummy_sensor: dummy_sensor::DummySensor = dummy_sensor::DummySensor::new(
        "continuous".to_string(),
        config.sensorname.clone(),
        config.location.clone(),
        config.sensortype,
    );
    // println!("iter: {}", config.continuous_mode_settings.send_Interval);
    let send_interval =
        std::time::Duration::from_secs(config.continuous_mode_settings.send_intervall as u64);
    let timepoint_offset: Duration = Duration::hours(1);
    let mut current_timepoint: DateTime<Utc> = config.start_datetime_utc();

    while !continuous_sensor_aborted.load(Ordering::SeqCst) {
        let current_time_str = current_timepoint.format("%F %T");
        try_send_log(
            &tx,
            thread_id,
            ThreadCallbackMessageKind::Info,
            format!("sending {}", current_time_str),
        );
        let w_measurement = my_dummy_sensor.take_measurement_at_safe(current_timepoint);
        try_send_log(
            &tx,
            thread_id,
            ThreadCallbackMessageKind::Trace,
            format!("{:?}", w_measurement),
        );
        match weatherprovider
            .add_weather_data_async(&w_measurement, false)
            .await
        {
            Ok(r) => match r.status().as_u16() {
                200..=299 => try_send_log(
                    &tx,
                    thread_id,
                    ThreadCallbackMessageKind::Trace,
                    r.status().to_string(),
                ),
                300..=399 => try_send_log(
                    &tx,
                    thread_id,
                    ThreadCallbackMessageKind::Warn,
                    r.status().to_string(),
                ),
                _ => try_send_log(
                    &tx,
                    thread_id,
                    ThreadCallbackMessageKind::Error,
                    r.status().to_string(),
                ),
            },
            Err(e) => {
                try_send_log(
                    &tx,
                    thread_id,
                    ThreadCallbackMessageKind::Error,
                    e.to_string(),
                );
                continuous_sensor_aborted.store(true, Ordering::SeqCst);
                try_send_log(
                    &tx,
                    thread_id,
                    ThreadCallbackMessageKind::Quit,
                    e.to_string(),
                );
            }
        };

        if !continuous_sensor_aborted.load(Ordering::SeqCst) {
            current_timepoint = current_timepoint + timepoint_offset;
            thread::sleep(send_interval);
        }
    }
    // println!("Sensor thread. Goodbye");
}

///
/// Listens to console text intput by the user.
/// Sends a callback of type Quit when the user types 'q' or 'quit'
///
    /// # Arguments
    /// * `tx` - Channel over which notifications should be send.
    /// * `thread_id` - Thread id.
pub async fn console_command_handler(
    tx: std::sync::mpsc::Sender<ThreadCallbackMessage>,
    thread_id: u32,
) {
    /// Will try to send an log message to the listener, will log to console if noone is listening anymore
    fn try_send_log(
        sender_pipe: &std::sync::mpsc::Sender<ThreadCallbackMessage>,
        sender_id: u32,
        msg_type: ThreadCallbackMessageKind,
        msg: String,
    ) {
        let to_send = ThreadCallbackMessage {
            sender_id,
            message_type: msg_type,
            message: msg,
        };
        match sender_pipe.send(to_send) {
            Ok(()) => {}
            Err(e) => {
                println!("An error occured while notifying caller thread: {}", e);
            }
        };
    }

    let mut running = true;
    while running {
        let stdin = io::stdin();
        let mut line = String::new();
        match stdin.read_line(&mut line).await {
            Ok(_) => {
                let len_withoutcrlf = line.trim_end().len(); // will trim also any ending whitespace!
                line.truncate(len_withoutcrlf);

                match line.as_str() {
                    "quit" => running = false,
                    "q" => running = false,
                    _ => { /* not a command do nothing */ }
                }

                if !running {
                    try_send_log(
                        &tx,
                        thread_id,
                        ThreadCallbackMessageKind::Quit,
                        String::new(),
                    );
                }
            }
            Err(e) => {
                try_send_log(
                    &tx,
                    thread_id,
                    ThreadCallbackMessageKind::Error,
                    e.to_string(),
                );
            }
        }
    }
    // println!("Console thread. Goodbye");
}
