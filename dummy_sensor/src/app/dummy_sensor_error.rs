use std::error::Error;
use std::fmt;

// ## Parse Error - Config

/// Type of error that occured during parsing of a configuration
#[derive(Debug)]
#[allow(dead_code)]
pub enum DummySensorConfigurationParseErrorKind {
    /// Error while parsing the config file.
    ParseError,
    /// IO Error e.g. File not found.
    IOError,
    /// Redirect Error from clap - Thrown when user entered `-h` or `--version`.
    CLIHelpOrVersionDisplayed,
    /// Argument is required but missing.
    MissingArgument {
        /// The missing argument.
        argument: String,
    },
    /// Arguent has invalid format.
    InvalidFormat {
        /// The Argument which is invalid.
        argument: String,
        /// Expected format.
        expeceted_format: String,
    },
}

impl Default for DummySensorConfigurationParseErrorKind {
    fn default() -> Self {
        DummySensorConfigurationParseErrorKind::ParseError
    }
}
/// Thrown when an error occured during configuration parsing
#[derive(Debug)]
pub struct DummySensorConfigurationParseError {
    /// Specific kind of error.
    pub kind: DummySensorConfigurationParseErrorKind,
    /// Additional information about the error
    pub message: String,
}
#[allow(dead_code)]
impl DummySensorConfigurationParseError {
    /// Creates a new DummySensorConfigurationParseError
    /// 
    /// # Arguments
    /// * `kind` - Type of error.
    /// * `message` - detailed error message.
    pub fn new(
        kind: DummySensorConfigurationParseErrorKind,
        message: String,
    ) -> DummySensorConfigurationParseError {
        DummySensorConfigurationParseError { kind, message }
    }

    /// Error kind of the error
    pub fn kind(&self) -> &DummySensorConfigurationParseErrorKind {
        &self.kind
    }
    /// Returns additional information
    pub fn message(&self) -> &String {
        &self.message
    }

    // # shortcut creation (only implemented for frequent errors)
    /// Creates an DummySensorConfigurationParseError with error kind InvalidFormat
    /// 
    /// # Arguments
    /// * `kind` - Type of error.
    /// * `message` - detailed error message.
    /// * `expected` - expected format.
    pub fn invalid_format(
        message: String,
        arg: String,
        expected: String,
    ) -> DummySensorConfigurationParseError {
        DummySensorConfigurationParseError {
            kind: DummySensorConfigurationParseErrorKind::InvalidFormat {
                argument: arg,
                expeceted_format: expected,
            },
            message,
        }
    }
    /// Creates an DummySensorConfigurationParseError with error kind MissingArgument
    /// 
    /// # Arguments
    /// * `kind` - Type of error.
    /// * `arg` - Missing argument.
    pub fn missing_argument(message: String, arg: String) -> DummySensorConfigurationParseError {
        DummySensorConfigurationParseError {
            kind: DummySensorConfigurationParseErrorKind::MissingArgument { argument: arg },
            message,
        }
    }
    /// Creates an DummySensorConfigurationParseError with error kind ParseError
    /// 
    /// # Arguments
    /// * `kind` - Type of error.
    /// * `message` - Detailed error message.
    pub fn parse_error(message: String) -> DummySensorConfigurationParseError {
        DummySensorConfigurationParseError {
            kind: DummySensorConfigurationParseErrorKind::ParseError,
            message,
        }
    }
}
impl fmt::Display for DummySensorConfigurationParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.kind {
            DummySensorConfigurationParseErrorKind::ParseError => {
                write!(f, "Dummy Sensor Parsing error. Message: {}", &self.message)
            }
            DummySensorConfigurationParseErrorKind::MissingArgument { argument: a } => write!(
                f,
                "Parsing error. Missing Argument '{}'. Message: {}",
                a, &self.message
            ),
            DummySensorConfigurationParseErrorKind::InvalidFormat {
                argument: a,
                expeceted_format: ef,
            } => write!(
                f,
                "Dummy Sensor Parsing error for argument '{}'. Expected: '{}'. Message: {}",
                a, ef, self.message
            ),
            DummySensorConfigurationParseErrorKind::IOError => write!(
                f,
                "Dummy Sensor IO Parsing error. Message: {}",
                &self.message
            ),
            DummySensorConfigurationParseErrorKind::CLIHelpOrVersionDisplayed => {
                write!(f, "{}", &self.message)
            }
            // _
            //     => write!(f, "Dummy Sensor Parsing error. Message: {}", self.message),
        }
    }
}

impl Error for DummySensorConfigurationParseError {
    fn description(&self) -> &str {
        &self.message
    }
}
