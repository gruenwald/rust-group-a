use chrono::{DateTime, NaiveDate, NaiveTime, Timelike, Utc};
use clap::load_yaml;
use clap::App;
use serde::Deserialize;
use serde::Serialize;

use super::configuration_serializers::*;
use super::dummy_sensor_error::*;
use config::Config;
use weather::weather_rest_api as api;
use weather_api as weather;

/// Default configuration path: "dummy_sensor.json"
const DEFAULT_CONFIG_PATH: &str = concat!(env!("CARGO_PKG_NAME"), ".json");
/// Default log level: 3 (info)
const DEFAULT_LOG_LEVEL: usize = 3;
/// Default load balancer url: "http://localhost"
const DEFAULT_DATABASE_URL: &str = "http://localhost";
/// Default load balancer port: 8000
const DEFAULT_PORT: u32 = 8000;
/// Default sensor type used in cli argument parsing: "random" (either random or temperature)
const DEFAULT_SENSOR_TYPE: &str = "random";
/// Default sensor name: "dummy_sensor"
const DEFAULT_SENSOR_NAME: &str = "dummy_sensor";
/// Default sensor location: "Munich"
const DEFAULT_LOCATION: &str = "Munich";
/// Default count of measurements to send in batch mode: 2
const DEFAULT_BATCH_COUNT: u32 = 2;
/// Default intervall between measurements in continuous mode: 7 seconds
const DEFAULT_CONTINUOUS_INTERVAL: u32 = 7;

///
/// Defines a run mode of the dummy sensor application. None means no mode is set.
/// In this case the app exits.
///
#[derive(Serialize, Deserialize, Copy, Clone, Debug)]
pub enum SensorRunMode {
    /// No mode defined
    None,
    /// Mode once - Send one measurement
    Once,
    /// Mode batch - Send multiple measurements
    Batch,
    /// Mode continuous - Send repeatedly measurements until stopped
    Continuous,
    /// Mode demo - Send Measurements for a day for multiple locations
    Demo,
}

///
/// Holds all information needed to execute the dummy sensor application.
///
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DummySensorConfiguration {
    /// Log level: 0-7 (0 = no log, 7 = Trace logs)
    pub log_level: usize,
    /// Mode the sensor is running.
    pub mode: SensorRunMode,
    /// The url under which the load balancer is reachable.
    pub lb_url: String,
    /// The port under wich the load balancer is listening on.
    pub lb_port: u32,
    /// Name of the Sensor.
    pub sensorname: String,
    /// Type of the sensor.
    pub sensortype: api::SensorType,
    /// Supposed location of the sensor.
    pub location: String,
    // private by intention,
    // Chrono Documentation says its best using chrono::NaiveDate instead of chrono::Date for serialization and deserialization uses
    // https://docs.rs/chrono/0.4.10/chrono/struct.Date.html
    /// date at which first generated measurement is dated.
    #[serde(with = "ymd_date_format")]
    start_date_utc: NaiveDate,

    // private by intention
    /// time at which first generated measurement is dated.
    #[serde(with = "h_time_format")]
    start_time_utc: NaiveTime,
    /// Special settings for once mode.
    pub once_mode_settings: OnceModeConfig,
    /// Special settings for batch mode.
    pub batch_mode_settings: BatchModeConfig,
    /// Special settings for continuous mode.
    pub continuous_mode_settings: ContinuousModeConfig,
    /// Special settings for demo mode.
    pub demo_mode_settings: DemoModeConfig,
}

impl Config<DummySensorConfiguration> for DummySensorConfiguration {}
/// Configuration of the once mode
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct OnceModeConfig {}
/// Configuration of the batch mode

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct BatchModeConfig {
    /// Count of measurements to send
    pub count: u32,
}
/// Configuration of the continuous mode
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ContinuousModeConfig {
    /// Interval between two measurments in seconds
    pub send_intervall: u32,
}
/// Configuration of the demo mode
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DemoModeConfig {}

// ### Implementations

impl DummySensorConfiguration {
    /// returns the datetime(utc) at which the first measurment should start
    pub fn start_datetime_utc(&self) -> DateTime<Utc> {
        DateTime::<Utc>::from_utc(self.start_date_utc.and_time(self.start_time_utc), Utc)
    }

    ///
    /// Create a config with default values
    /// Sensortype will be Temperature
    ///
    pub fn generate_default_config() -> DummySensorConfiguration {
        let now = Utc::now().naive_utc();
        DummySensorConfiguration {
            log_level: DEFAULT_LOG_LEVEL,
            mode: SensorRunMode::None,
            lb_url: DEFAULT_DATABASE_URL.to_string(),
            lb_port: DEFAULT_PORT,
            sensorname: DEFAULT_SENSOR_NAME.to_string(),
            sensortype: api::SensorType::Temperature,
            location: DEFAULT_LOCATION.to_string(),
            start_date_utc: now.date(),
            start_time_utc: NaiveTime::from_hms(now.time().hour(), 0, 0),
            once_mode_settings: OnceModeConfig {},
            batch_mode_settings: BatchModeConfig {
                count: DEFAULT_BATCH_COUNT,
            },
            continuous_mode_settings: ContinuousModeConfig {
                send_intervall: DEFAULT_CONTINUOUS_INTERVAL,
            },
            demo_mode_settings: DemoModeConfig {},
        }
    }

    // # IO

    ///
    /// Create/Loads a DummySensorConfiguration from either a config file or cli arguments.
    /// When the flag -c <path> or --configuration <path> is set the configuration will be loaded from the given file.
    ///  
    /// # Arguments
    /// * `program_version` - Program version, that will appear in the cli helptext.
    /// * `try_default_config` - if true the default config will be loaded when no arguments and subcommands are provided.
    pub fn from_cli_arguments_or_config(
        program_version: &str,
        try_default_config: bool,
    ) -> Result<DummySensorConfiguration, DummySensorConfigurationParseError> {
        let yaml = load_yaml!("cli.yml");
        let matches;

        match App::from_yaml(yaml)
            .version(program_version)
            .get_matches_safe()
        {
            Ok(m) => matches = m,
            Err(clap::Error {
                kind: clap::ErrorKind::HelpDisplayed,
                message: e,
                ..
            }) => {
                return Err(DummySensorConfigurationParseError::new(
                    DummySensorConfigurationParseErrorKind::CLIHelpOrVersionDisplayed,
                    e,
                ))
            }
            Err(clap::Error {
                kind: clap::ErrorKind::VersionDisplayed,
                message: e,
                ..
            }) => {
                return Err(DummySensorConfigurationParseError::new(
                    DummySensorConfigurationParseErrorKind::CLIHelpOrVersionDisplayed,
                    e,
                ))
            }

            Err(e) => {
                return Err(DummySensorConfigurationParseError::new(
                    DummySensorConfigurationParseErrorKind::IOError,
                    e.to_string(),
                ))
            }
        }

        if try_default_config {
            let keys_cnt = matches.args.keys().len();
            if keys_cnt == 0 {
                // no arguments provided
                if matches.subcommand.is_none() {
                    // also no subcommands provided
                    println!(
                        "no arguments and subcommand provided, loading configuration from '{}'",
                        DEFAULT_CONFIG_PATH
                    );
                    return Ok(DummySensorConfiguration::from_configuration_file(
                        DEFAULT_CONFIG_PATH,
                    )?);
                }
            }
        }

        if let Some(config_path) = matches.value_of("configuration") {
            println!("loading configuration from '{}'", config_path);
            Ok(DummySensorConfiguration::from_configuration_file(
                config_path,
            )?)
        } else {
            Ok(DummySensorConfiguration::from_clap_matches(&matches)?)
        }
    }

    ///
    /// Creates as DummySensorConfiguration from Clap CLI Arguments
    /// Note: Ignores the configuration flag, so no configuration from file is loaded.
    /// If this is whished please use from_configuration_file() directly or from_cli_arguments_or_config()
    ///
    /// # Arguments
    /// * `matches` - Clap matches that will be used to parse the configuration
    pub fn from_clap_matches(
        matches: &clap::ArgMatches,
    ) -> Result<DummySensorConfiguration, DummySensorConfigurationParseError> {
        let mut config = DummySensorConfiguration::generate_default_config();

        match matches.value_of("verbosity") {
            Some(verbosity) => {
                config.log_level = verbosity.parse::<usize>().map_err(|e| {
                    DummySensorConfigurationParseError::invalid_format(
                        e.to_string(),
                        "verbosity".to_string(),
                        "VERBOSITY".to_string(),
                    )
                })?
            }
            None => config.log_level = DEFAULT_LOG_LEVEL,
        }

        let _url_raw = matches.value_of("url").unwrap_or(DEFAULT_DATABASE_URL); // if not set use default value
        if let Some(p) = matches.value_of("port") {
            config.lb_port = p.parse::<u32>().map_err(|e| {
                DummySensorConfigurationParseError::invalid_format(
                    e.to_string(),
                    "port".to_string(),
                    "Integer".to_string(),
                )
            })?;
        } else {
            config.lb_port = DEFAULT_PORT;
        }

        let _sensor_type_raw = matches
            .value_of("sensortype")
            .unwrap_or(DEFAULT_SENSOR_TYPE);
        let _sensorname_raw = matches
            .value_of("sensorname")
            .unwrap_or(DEFAULT_SENSOR_NAME);
        let _location_raw = matches.value_of("location").unwrap_or(DEFAULT_LOCATION);

        config.lb_url = _url_raw.to_string();

        match _sensor_type_raw.to_lowercase().as_ref() {
            "humidity" => config.sensortype = api::SensorType::Humidity,
            "temperature" => config.sensortype = api::SensorType::Temperature,
            "random" => {
                // short for rand::thread_rng().gen()
                if rand::random() {
                    config.sensortype = api::SensorType::Temperature
                } else {
                    config.sensortype = api::SensorType::Humidity
                }
            }
            _ => {
                return Err(DummySensorConfigurationParseError::invalid_format(
                    String::new(),
                    "sensortype".to_string(),
                    "Sensortype".to_string(),
                ));
            }
        }
        config.sensorname = _sensorname_raw.to_string();
        if _location_raw.trim_end().chars().count() == 0 {
            return Err(DummySensorConfigurationParseError::invalid_format(
                "Invalid sensorname".to_string(),
                "sensorname".to_string(),
                "String".to_string(),
            ));
        } else {
            config.location = _location_raw.to_string();
        }

        if let Some(arg_date) = matches.value_of("startdate") {
            config.start_date_utc =
                NaiveDate::parse_from_str(arg_date, "%Y-%m-%d").map_err(|e| {
                    DummySensorConfigurationParseError::invalid_format(
                        e.to_string(),
                        "startdate".to_string(),
                        "yyyy-mm-dd".to_string(),
                    )
                })?;
        } else {
            config.start_date_utc = Utc::now().date().naive_utc();
        }

        if let Some(a_time) = matches.value_of("starttime") {
            let hour = a_time.parse::<u32>().map_err(|e| {
                DummySensorConfigurationParseError::invalid_format(
                    e.to_string(),
                    "starttime".to_string(),
                    "HH (0-23)".to_string(),
                )
            })?;
            match NaiveTime::from_hms_opt(hour, 0, 0) {
                Some(time) => config.start_time_utc = time,
                None => {
                    return Err(DummySensorConfigurationParseError::invalid_format(
                        String::new(),
                        "starttime".to_string(),
                        "HH (0-23)".to_string(),
                    ));
                }
            }
        } else {
            let now_time: NaiveTime = Utc::now().naive_utc().time();
            config.start_time_utc = NaiveTime::from_hms(now_time.hour(), 0, 0);
        }

        //
        // # parse subcommands
        //
        match matches.subcommand() {
            ("once", Some(_sub_m)) => {
                config.mode = SensorRunMode::Once;
            }
            ("batch", Some(_sub_m)) => {
                config.mode = SensorRunMode::Batch;
                match _sub_m.value_of("count") {
                    Some(batch_count_raw) => {
                        config.batch_mode_settings.count =
                            batch_count_raw.parse::<u32>().map_err(|e| {
                                DummySensorConfigurationParseError::invalid_format(
                                    e.to_string(),
                                    "count".to_string(),
                                    "Integer".to_string(),
                                )
                            })?
                    }
                    None => config.batch_mode_settings.count = DEFAULT_BATCH_COUNT,
                }
            }
            ("continuous", Some(_sub_m)) => {
                config.mode = SensorRunMode::Continuous;
                match _sub_m.value_of("interval") {
                    Some(batch_count_raw) => {
                        let parsed = batch_count_raw.parse::<u32>().map_err(|e| {
                            DummySensorConfigurationParseError::invalid_format(
                                e.to_string(),
                                "interval".to_string(),
                                "Integer > 0".to_string(),
                            )
                        })?;
                        if parsed > 0 {
                            config.continuous_mode_settings.send_intervall = parsed;
                        } else {
                            return Err(DummySensorConfigurationParseError::invalid_format(
                                String::new(),
                                "interval".to_string(),
                                "Integer > 0".to_string(),
                            ));
                        }
                    }
                    None => {
                        config.continuous_mode_settings.send_intervall = DEFAULT_CONTINUOUS_INTERVAL
                    }
                }
            }
            ("demo", Some(_sub_m)) => {
                config.mode = SensorRunMode::Demo;
            }
            _ => {
                // Either no subcommand or one not tested for ...
                config.mode = SensorRunMode::None;
            }
        }

        Ok(config)
    }

    ///
    /// Loads a configuration file from disk and creates a DummySensorConfiguration.
    /// 
    /// # Arguments
    /// * `filepath` - Path of configuration file.
    pub fn from_configuration_file(
        filepath: &str,
    ) -> Result<DummySensorConfiguration, DummySensorConfigurationParseError> {
        Self::new_from_file(&filepath).map_err(|e| {
            DummySensorConfigurationParseError::new(
                DummySensorConfigurationParseErrorKind::IOError,
                e.to_string(),
            )
        })
    }
}
