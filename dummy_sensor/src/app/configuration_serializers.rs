///
/// Serde JSON Parser to serialize/deserialize an Chrono:Naivetime into/from a string in the format 'hh'.
/// Additionally also allows to deserialize the string "now" as the current hour.
///
pub mod h_time_format {
    use chrono::{NaiveTime, Timelike, Utc};
    use serde::{self, Deserialize, Deserializer, Serializer};

    /// Time format in strftime
    const FORMAT: &str = "%H"; // hour "00"

    ///
    /// Serializes NaiveTime as an String with the format "%T"
    /// Example: "19:20:00" -> NaiveTime
    ///
    pub fn serialize<S>(time: &NaiveTime, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", time.format(FORMAT));
        serializer.serialize_str(&s)
    }

    ///
    /// Deserialize an String with the format "%T" as an NaiveTime
    /// Example: NaiveTime -> "19:20:00"
    /// Additionally also allows to deserialize the string "now" as the current hour.
    ///
    pub fn deserialize<'de, D>(deserializer: D) -> Result<NaiveTime, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;

        if "now" == s.to_lowercase() {
            Ok(NaiveTime::from_hms(
                Utc::now().naive_utc().time().hour(),
                0,
                0,
            ))
        } else {
            let hour = s.parse::<u32>().map_err(|e| {
                serde::de::Error::custom(format!("Invalid Time Format. Expect hh. Detail: {}", e))
            })?;
            if hour > 23 {
                return Err(serde::de::Error::custom(
                    "Invalid Time Format. Expect hh (0-23).".to_string(),
                ));
            }
            match NaiveTime::from_hms_opt(hour, 0, 0) {
                Some(t) => Ok(t),
                None => Err(serde::de::Error::custom(
                    "Invalid Time Format. Expect hh (0-23).".to_string(),
                )),
            }
        }
    }
}

///
/// Serde JSON Parser to serialize/deserialize an Chrono:NaiveDate into/from a string in the format 'YYYY-MM-DD'.
/// Additionally also allows to deserialize the string "today" as the current date.
///
pub mod ymd_date_format {
    use chrono::{NaiveDate, Utc};
    use serde::{self, Deserialize, Deserializer, Serializer};

    /// Time format in strftime
    const FORMAT: &str = "%F"; // date "YYYY-MM-DD" same as %Y-%m-%d

    ///
    /// Serializes NaiveDate as an String with the format "%F"
    /// Example: "2020-05-20" -> NaiveDate
    ///
    pub fn serialize<S>(time: &NaiveDate, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", time.format(FORMAT));
        serializer.serialize_str(&s)
    }

    ///
    /// Deserialize an String with the format "%F" as an NaiveDate
    /// Example: NaiveDate -> "2020-05-20"
    /// Additionally also allows to deserialize the string "today" as the current date.
    ///
    pub fn deserialize<'de, D>(deserializer: D) -> Result<NaiveDate, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;

        if "today" == s.to_lowercase() {
            Ok(Utc::now().naive_utc().date())
        } else {
            Ok(NaiveDate::parse_from_str(&s, FORMAT).map_err(|e| {
                serde::de::Error::custom(format!(
                    "Invalid Date Format. Expect yyyy-mm-dd. Detail: {}",
                    e
                ))
            })?)
        }
    }
}
