# The Dummy Sensor Application

This Application can generate random temperature and humidity measurements
and sends them to the server (LoadBalancer)
There are 4 modes of execution (Once, Batch, Continuous and Demo)

## How to Run

While being in the ./dummy_sensor/ directory

1. >`cargo build`
2. >`cargo run -- [OPTIONS] [SUBCOMMAND]`

`cargo run --` can be exchanged with the executable name `dummy_sensor.exe`.

The dummy sensor can be started in 2 ways

1. with commandline arguments `dummy_sensor.exe [OPTIONS] [SUBCOMMAND]`:
2. or with a configuration file (ignoring any set arguments):  
     `dummy_sensor.exe` (will try to load ./dummy_sensor.json)  
     or  
     `dummy_sensor.exe -c "someother_config.json"`

## CLI Arguments Examples

For full argument instructions see chapter `Command Line Arguments` or run

>`dummy_sensor.exe -h`

### Overview Modes

**Once** - send one measurement

> `dummy_sensor.exe once`

**Batch** - Generate and send 4 measurements**

>`dummy_sensor.exe  batch -c 4`

**Continuously** - Generate and send measurement a measurement every 7 seconds (measurements timestamps will be 1 hour apart)

>`dummy_sensor.exe continuously -i 7`

**Demo** - Generate and send temperature measurements for today for all locations (Munich, Berlin, Cologne, Hamburg and Erfurt). 

>`dummy_sensor.exe demo`

### Example Adjustments

Define Url and/or Port: `dummy_sensor.exe -u "http://141.84.91.111" -p 4502 once`

Change date and/or time of measurement: `dummy_sensor.exe --startdate "2020-01-20" --starttime 15 batch`

Start with configuration file: `dummy_sensor.exe -c "config.json"` (Note all further arguments will be ignored)

## Command Line Arguments

### Basic Arguments

| short | long    | argument              | description |
| ----- | -----   | -----                 | -----       |
|  h    | help    |                       | Prints help information    |
|  V    | version |                       | Prints version information |
|  v    |         |  level: 0-7                  | Sets the level of verbosity (log level) |
|  c    | config  | configuration filepath | Start Dummy Sensor with settings from configuration File. <br> All other arguments set, e.g. url or sensortype, will be ignored. |
|  u    | url     | url           | Url of database-server starting with 'http://' e.g. 'http://141.84.91.111' (default 'http://localhost'). |
|  p    | port    | port          | Port of database-server (default 8000) |
|  t    | type    | humitdity, temperature, random       | Type of sensor |
|  n    | name    | String        | Name of sensor |
|  l    | loc     | location      | Location of sensor e.g. "Munich" |
|       | startdate   | date      | Define the date (utc) of the first measurement. Format 'YYYY-mm-dd'. If not set current date will be used. |
|       | starttime   | hour      | Hour (utc) at which first measurement should start. Format 'HH'. If not set current hour will be used. |

### Subcommands (Modes)

| subcomand   | argument short  | long  | description |
| -----       | ----- | -----           | -----       |
| once        |       |                 | Send one measurement |
| batch       |       |                 | Create and send multiple measurements |
|             | c     | count           | Number of measurements to create and send |
| continuous  |       |                 | Sends continuously, in a defined interval, measurements. Timestamps will increase by 1 hour each time.   |
|             | i     | interval        | Send interval in seconds |
| demo        |       |                 | Generate temperature measurements for all locations. <br> Ignores all arguments except of port, url and startdate|

## Configuration File

All settings made over the cli interface can also be defined in an configuration file. 
This allows to predefine multiple modes and to switch them by the changing configuration file.
For an example see chaper `Example Configuration File`

The dummy sensor can be told to start with a configuration in two ways:
1. `dummy_sensor.exe` (no arguments supplied -> will try to load ./dummy_sensor.json)  
2. `dummy_sensor.exe -c "someother_config.json"` (all other arguments provided will be ignored)

**Notes:**
Some settings expect certain values:

- The setting `mode` expects: `None`, `Once`, `Batch`, `Continuous` or `Demo`
- The setting `sensortype` expects: `Humidity` or `Temperature`
- The setting `start_date_utc` expects the date in the format `yyyy-mm-dd` or `today`
- The setting `start_time_utc` expects the hour to start (0-23) e.g. `13` or `now`

### Example Configuration File

Example congfig: will send 20 Measurements, starting at midnight

```json
{
  "log_level": 7,
  "mode": "Batch",
  "lb_url": "http://localhost",
  "lb_port": 8000,
  "sensorname": "test_batch",
  "sensortype": "Temperature",
  "location": "Munich",
  "start_date_utc": "today",
  "start_time_utc": "0",
  "once_mode_settings": {},
  "batch_mode_settings": {
    "count": 20
  },
  "continuous_mode_settings": {
    "send_intervall": 7
  },
  "demo_mode_settings": {}
}
```

this is equal to

`dummy_sensor.exe -v 7 -n "test_batch" -t "Temperature" -l "Munich" --starttime 0 batch -c 20`

## Used Crates

- `async-std` for async io
- `chrono` for date and time
- `clap` for command line arguments
- `log`, `stderrlog` logging
- `rand` random numbers
- `reqwest` for http requests
- `serde`, `serde_json`, `serde_derive` for json parsing
- `tokio` async runtime
-  `weather_api` library, created by us, to send our WeatherMeasuerments 
- `config` library, created by us, to abstract configuration file handling

