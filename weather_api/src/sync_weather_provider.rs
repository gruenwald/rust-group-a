use super::weather_provider_error::{WeatherProviderError, WeatherProviderErrorKind};

use chrono::{Date, Utc};
use reqwest;

/// Provides an interface to store and retrieve weather data from a database.
/// This is the blocking version for synchronous libraries like Rocket.
pub struct SyncWeatherProvider {
    /// The url of the datase to which measurements should be send.
    db_url: String,
    /// The port of the datase to which measurements should be send.
    db_port: u32,
    /// The route to which measurements can be send.
    route_post_measurement: String,
    /// The route to which measurements can be retrieved.
    route_get_measurement: String,
    /// The http client which is used to send measurements
    http_client: reqwest::blocking::Client,
}

impl SyncWeatherProvider {
    fn get_request_url(&self, request_type: super::RequestType, key: &str) -> String {
        let route = match request_type {
            super::RequestType::Get => &self.route_get_measurement,
            super::RequestType::Post => &self.route_post_measurement,
        };
        format!("http://{}:{}{}/{}", self.db_url, self.db_port, route, key)
    }

    /// Creates a new Synchronous weather provider
    /// # Arguments
    /// * `db_url` - The ip or dns name of the db server
    /// * `port` - The port of the db server
    pub fn new(db_url: &str, port: u32) -> SyncWeatherProvider {
        SyncWeatherProvider {
            db_url: String::from(db_url),
            db_port: port,
            route_post_measurement: String::from("/save"), // /save/<key>
            route_get_measurement: String::from(""),       // /load/<key>
            http_client: reqwest::blocking::Client::new(),
        }
    }

    /// Retrieves the latest weather data of the given location and sensor type
    /// # Arguments
    /// * `location_name` - The name of the location
    /// * `sensor_type` - The type of the sensor (temperature or humidity)
    pub fn get_latest_weather_data(
        &self,
        location_name: &str,
        sensor_type: super::weather_rest_api::SensorType,
    ) -> Result<super::weather_rest_api::WeatherDataMessage, WeatherProviderError> {
        let key = format!(
            "{}.{}.{}",
            location_name,
            &sensor_type.to_string(),
            "latest"
        );
        Ok(self.get_result_obj(&key)?)
    }

    /// Retrieves a list of weather data from a given day
    /// # Arguments
    /// * `location_name` - The name of the location
    /// * `sensor_type` - The type of the sensor (temperature or humidity)
    /// * `date` - The date of the day
    pub fn get_weather_data_by_date(
        &self,
        location_name: &str,
        sensor_type: super::weather_rest_api::SensorType,
        date: Date<Utc>,
    ) -> Result<Vec<super::weather_rest_api::WeatherDataMessage>, WeatherProviderError> {
        let key = format!(
            "search/{}.{}.{}",
            location_name,
            &sensor_type.to_string(),
            date.format("%Y%m%d")
        );
        Ok(self.get_result_objects(&key)?)
    }

    fn get_result_objects(
        &self,
        key: &str,
    ) -> Result<Vec<super::weather_rest_api::WeatherDataMessage>, WeatherProviderError> {
        let mut result = Vec::new();
        let response = &self.get_result_string(&key)?;
        let key_value_pairs = serde_json::from_str::<Vec<(String, String)>>(response.as_str())
            .map_err(|e| {
                WeatherProviderError::new(WeatherProviderErrorKind::ParseError, e.to_string())
            })?;
        for x in key_value_pairs.iter() {
            result.push(
                serde_json::from_str::<super::weather_rest_api::WeatherDataMessage>(&x.1).map_err(
                    |e| {
                        WeatherProviderError::new(
                            WeatherProviderErrorKind::ParseError,
                            e.to_string(),
                        )
                    },
                )?,
            )
        }
        Ok(result)
    }

    fn get_result_obj(
        &self,
        key: &str,
    ) -> Result<super::weather_rest_api::WeatherDataMessage, WeatherProviderError> {
        let result = &self.get_result_string(&key)?;
        let deser = serde_json::from_str(result.as_str());
        match deser {
            Ok(obj) => Ok(obj),
            Err(e) => Err(WeatherProviderError::new(
                WeatherProviderErrorKind::ParseError,
                e.to_string(),
            )),
        }
    }

    fn get_result_string(&self, key: &str) -> Result<String, WeatherProviderError> {
        println!("key: {}", key);
        let url = &self.get_request_url(super::RequestType::Get, &key);
        println!("Url: {}", &url);
        Ok(self
            .http_client
            .get(url)
            .send()
            .map_err(|e| {
                WeatherProviderError::new(WeatherProviderErrorKind::GetError, e.to_string())
            })?
            .text()
            .map_err(|e| {
                WeatherProviderError::new(WeatherProviderErrorKind::ParseError, e.to_string())
            })?)
    }
}
