/// Async weather data provider.
pub mod async_weather_provider;
/// Synchronous weather provider.
pub mod sync_weather_provider;
/// Possible errors during data collection.
pub mod weather_provider_error;
/// Structures for saving the data in the database.
pub mod weather_rest_api;

use serde::{Deserialize, Serialize};
use weather_provider_error::{WeatherProviderError, WeatherProviderErrorKind};
/// All locations for the demo applicaiton.
pub static LOCATIONS: [&str; 5] = ["Munich", "Berlin", "Cologne", "Hamburg", "Erfurt"];

/// Http request types.
#[derive(Serialize, Deserialize, Debug)]
pub enum RequestType {
    /// Http Get
    Get,
    /// Http Post
    Post,
}

///
/// Builds a key for a measurement to store it in the database
/// The form is <locName>.<SensorType>.[latest | <Date>.<Hour>]
/// # Arguments
/// * `measurement` - The measurement you want to build the key for
/// * `is_latest` - If set to true, the date and hour at the end are substituted by "latest"
fn build_key(
    measurement: &weather_rest_api::WeatherDataMessage,
    is_latest: bool,
) -> Result<String, WeatherProviderError> {
    let sensor_type_value = serde_json::to_value(&measurement.sensor_type).map_err(|e| {
        WeatherProviderError::new(WeatherProviderErrorKind::KeyError, e.to_string())
    })?;

    let sensortype_str;
    match sensor_type_value.as_str() {
        Some(t) => sensortype_str = t,
        None => {
            return Err(WeatherProviderError::new(
                WeatherProviderErrorKind::KeyError,
                From::from("Sensortype could not be parsed!"),
            ))
        }
    }

    let time_subkey = if is_latest {
        "latest".to_string()
    } else {
        measurement.date_time.format("%Y%m%d.%H").to_string()
    };

    Ok(format!(
        "{}.{}.{}",
        &measurement.location, &sensortype_str, time_subkey
    ))
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{DateTime, NaiveDate, Utc};

    #[test]
    fn test_key() {
        let message = super::weather_rest_api::WeatherDataMessage {
            sensor_type: super::weather_rest_api::SensorType::Temperature,
            location: String::from("testLoc"),
            temperature: Some(5),
            temperature_unit: Some(super::weather_rest_api::TemperatureUnit::Celsius),
            humidity: None,
            date_time: DateTime::<Utc>::from_utc(
                NaiveDate::from_ymd(2020, 1, 1).and_hms(12, 0, 0),
                Utc,
            ),
        };

        let key = build_key(&message, false).unwrap();
        let key_latest = build_key(&message, true).unwrap();

        assert_eq!(key, "testLoc.Temperature.20200101.12");
        assert_eq!(key_latest, "testLoc.Temperature.latest");
    }
}
