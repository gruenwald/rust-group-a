use std::error::Error;
use std::fmt;

/// Possible kinds of errors.
#[derive(Debug)]
#[allow(dead_code)]
pub enum WeatherProviderErrorKind {
    /// General Error. For all error that cannot be mapped accordingly.
    GeneralError,
    /// Error while posting to database.
    PostError,
    /// Key not found.
    KeyError,
    /// Error while getting from database.
    GetError,
    /// Error while parsing the result.
    ParseError,
}
impl Default for WeatherProviderErrorKind {
    fn default() -> Self {
        WeatherProviderErrorKind::GeneralError
    }
}
/// Errortype for weather data requests.
#[derive(Debug)]
pub struct WeatherProviderError {
    /// What kind of error occurred.
    kind: WeatherProviderErrorKind,
    /// The exact error message
    message: String,
}

#[allow(dead_code)]
impl WeatherProviderError {
    /// Creates a new weather provider error
    /// # Arguments
    /// * `kind` - What kind of error occurred.
    /// * `message` - The exact error message.
    pub fn new(kind: WeatherProviderErrorKind, message: String) -> WeatherProviderError {
        WeatherProviderError { kind, message }
    }
    /// Returns the error message
    pub fn message(&self) -> &String {
        &self.message
    }
}

impl fmt::Display for WeatherProviderError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Weather Provider Error: {}", self.message)
    }
}

impl Error for WeatherProviderError {
    fn description(&self) -> &str {
        &self.message
    }
}
