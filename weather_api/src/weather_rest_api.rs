use chrono::{DateTime, Utc};
use serde::Deserialize;
use serde::Serialize;
use std::fmt;

/// The type of the sensor.
#[derive(Serialize, Deserialize, Copy, Clone, Debug)]
pub enum SensorType {
    /// Humidity sensor.
    Humidity,
    /// Temperature sensor.
    Temperature,
}

impl Default for SensorType {
    fn default() -> Self {
        SensorType::Temperature
    }
}

impl fmt::Display for SensorType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

/// The unit for the temperature
#[derive(Serialize, Deserialize, Copy, Clone, Debug)]
pub enum TemperatureUnit {
    /// Degree Celsius.
    Celsius,
    /// Fahrenheit.
    Fahrenheit,
}

// https://serde.rs/field-attrs.html
/// Represents a weather message.
/// Is used to wrap sensor data and store it in the database.
#[derive(Serialize, Deserialize, Debug)]
pub struct WeatherDataMessage {
    /// The sensor type. Decides if temperature/humidity are Some() or None.
    #[serde(default)]
    pub sensor_type: SensorType,
    /// The name of the location, e.g. Munich.
    #[serde(default)]
    pub location: String,
    /// The temperature, only set if the sensor type is Temperature.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub temperature: Option<i32>,
    /// The temperature unit, only set if the sensor type is Temperature.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub temperature_unit: Option<TemperatureUnit>,
    /// The humidity, only set if the sensor type is Humidty.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub humidity: Option<i32>,
    /// The time (Utc) at which the measurement was taken.
    #[serde(default = "Utc::now")]
    pub date_time: DateTime<Utc>,
}
