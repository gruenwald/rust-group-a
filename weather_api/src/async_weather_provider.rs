use super::weather_provider_error::{WeatherProviderError, WeatherProviderErrorKind};

use chrono::{DateTime, Utc};
use log::*;
use reqwest;
use rest_data::DataWrapper;

/// Provides an async interface to store weather data from a database.
/// This is the non blocking version for non async libraries like Tokio.
pub struct AsyncWeatherProvider {
    /// The url of the datase to which measurements should be send.
    db_url: String,
    /// The port of the datase to which measurements should be send.
    db_port: u32,
    /// The route to which measurements can be send.
    route_post_measurement: String,
    /// The route to which measurements can be retrieved.
    route_get_measurement: String,
    /// The http client which is used to send measurements
    http_client: reqwest::Client,
}

impl AsyncWeatherProvider {
    fn get_request_url(&self, request_type: super::RequestType, key: &str) -> String {
        let route = match request_type {
            super::RequestType::Get => &self.route_get_measurement,
            super::RequestType::Post => &self.route_post_measurement,
        };
        format!("{}:{}{}/{}", self.db_url, self.db_port, route, key)
    }

    /// Creates a new Asynchronous weather provider
    /// # Arguments
    /// * `db_url` - The ip or dns name of the db server
    /// * `port` - The port of the db server
    pub fn new(db_url: &str, port: u32) -> AsyncWeatherProvider {
        AsyncWeatherProvider {
            db_url: String::from(db_url),
            db_port: port,
            route_post_measurement: String::from("/save"), // /save/<key>
            route_get_measurement: String::from(""),       // /load/<key>
            http_client: reqwest::Client::new(),
        }
    }

    /// Sends a WeatherDataMessage to the Load Balancer.
    /// Also in this case the response of the request with the more severe http Statuscode is returned
    /// # Arguments
    /// * `measurement` - The measurement you want to store
    /// * `is_latest` - When set to true, an additional request is issued under a key with timestamp 'latest' -> for ui
    pub async fn add_weather_data_async(
        &self,
        measurement: &super::weather_rest_api::WeatherDataMessage,
        is_latest: bool,
    ) -> Result<reqwest::Response, WeatherProviderError> {
        let json_parse_message = serde_json::to_string_pretty(&measurement).map_err(|e| {
            WeatherProviderError::new(WeatherProviderErrorKind::ParseError, e.to_string())
        })?;
        let wrapped_message =
            serde_json::to_string_pretty(&DataWrapper::new_from_str(&json_parse_message))
                .expect("Couldn't serialize measurement");
        info!("Sending wrapped message {}", wrapped_message);

        let key = super::build_key(&measurement, false)?;
        let req_url = &self.get_request_url(super::RequestType::Post, &key);
        info!("Sending to {}", req_url);

        //actual timestamp as key
        let repsonse_time = self
            .http_client
            .post(req_url)
            .body(wrapped_message.clone())
            .send()
            .await
            .map_err(|e| {
                WeatherProviderError::new(WeatherProviderErrorKind::PostError, e.to_string())
            })?;
        if !is_latest {
            return Ok(repsonse_time);
        }
        //latest as key
        let latest_key = super::build_key(&measurement, true)?;
        let req_url_latest = &self.get_request_url(super::RequestType::Post, &latest_key);
        info!("Sending to {}", req_url_latest);
        //TODO: maybe only do this if specified as a parameter
        let repsonse_latest = self
            .http_client
            .post(req_url_latest)
            .body(wrapped_message.clone())
            .send()
            .await
            .map_err(|e| {
                WeatherProviderError::new(WeatherProviderErrorKind::PostError, e.to_string())
            })?;

        if repsonse_latest.status().as_u16() >= repsonse_time.status().as_u16() {
            Ok(repsonse_latest)
        } else {
            Ok(repsonse_time)
        }
    }

    /// Retrieves a list of weather data at a specific point in time
    /// # Arguments
    /// * `location_name` - The name of the location
    /// * `sensor_type` - The type of the sensor (temperature or humidity)
    /// * `date` - The date and time of the day. Rounded by hour
    pub async fn get_weather_data_by_datetime_async(
        &self,
        location_name: &str,
        sensor_type: super::weather_rest_api::SensorType,
        date: DateTime<Utc>,
    ) -> Result<super::weather_rest_api::WeatherDataMessage, WeatherProviderError> {
        let key = format!(
            "{}.{}.{}",
            location_name,
            &sensor_type.to_string(),
            date.format("%Y%m%d.%H")
        );
        Ok(self.get_result_async(&key).await?)
    }

    async fn get_result_async(
        &self,
        key: &str,
    ) -> Result<super::weather_rest_api::WeatherDataMessage, WeatherProviderError> {
        info!("key: {}", key);
        let body = reqwest::get(&self.get_request_url(super::RequestType::Get, &key))
            .await
            .map_err(|e| {
                WeatherProviderError::new(WeatherProviderErrorKind::GetError, e.to_string())
            })?
            .text()
            .await
            .map_err(|e| {
                WeatherProviderError::new(WeatherProviderErrorKind::ParseError, e.to_string())
            })?;
        let parse_weather_message: super::weather_rest_api::WeatherDataMessage =
            serde_json::from_str(&body).map_err(|e| {
                WeatherProviderError::new(WeatherProviderErrorKind::ParseError, e.to_string())
            })?;
        Ok(parse_weather_message)
    }
}
