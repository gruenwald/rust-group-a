# Our Project

Our goal is to implement a distributed key value data base that can store arbitrary data.  

## Key-Value Database

- As far as the user is concerned the Database should appear as a single instance.
- Adding a key-value set to the database.
- Querying data by a fully qualified key.
- Querying a data list by a partial key.
- Deleting a data set by a partial key.
- Deleting a single value by a fully qualified key.

## Demo Application - Weather App

The demo application presents weather data for selected weather sensors via a website.

The data it presents is randomly generated by a set of dummy sensors for presentation purposes.  
The GUI displays a map showing the current (latest) temperature recorded at a location.  
A graph of the temperature/humidity at a location during a day can be shown on click.  

![image.png](https://gitlab2.cip.ifi.lmu.de/gruenwald/rust-group-a/uploads/64065c110bfd68a1fed8cf1add80deb5/image.png)	

## What is working

All key features of the database are implemented. In addition the demo application and the dummy sensor are able to use the database for the intended purpose.

## Next steps

We are aware of several issues that do not interfere with the base functionality, but would be nice to have for a mature product.  
For details, please refer to the [open issues on GitLab](https://gitlab2.cip.ifi.lmu.de/gruenwald/rust-group-a/issues)

## Architecture

![architecture.svg](https://gitlab2.cip.ifi.lmu.de/gruenwald/rust-group-a/uploads/07caf532dfd6f3d301fe9d674fa16ddd/architecture.svg)

The Database itself is structured like a tree. Subkeys are separated by dots (e.g. `key1.key1_1.key1_1_3`) for `request::Insert` as well as `request::GetByFullyQualifiedKey` and `request::GetByPartialKey`. The Advantages are that it is possible to retrieve all nodes under a specific subkey, e.g. from one day/sensor/location.


![tree.png](https://gitlab2.cip.ifi.lmu.de/gruenwald/rust-group-a/uploads/2634dff97a0b8e775c8f4bf4ad7e43e3/Database.png)

## Building and Running the Application(-s)

Some parts of the application (the parts using the Rocket crate) **require the nightly version** of Rust.  
Make sure to switch your environment accordingly by [following the official instructions.](https://github.com/rust-lang/rustup#working-with-nightly-rust)

### Building the Application(-s)

Run `cargo build` or `cargo build --release` in the `rust-group-a/` directory.

### Running the Application(-s)

1. Each application requires a configuration file named like the application crate, e.g. `load_balancer.json`.  
   Make sure that all parameters are set appropriately for your environment. Please refer to the documentation/provided readme files for details on the parameters for each application.  
   The configurations provided are set up in such a way that the applications can be run locally.
2. Start the load balancer either via `cargo run` in the `load_balancer/` directory, or via the compiled application.
3. Start at least one database instance either via `cargo run` in the `database/` directory, or via the compiled application.
4. Write data into the database via the dummy sensor, either via `cargo run demo` in the `dummy_sensor` or via the compiled application. For other run modes and more detailed instructions, please refer to the provided readme.
5. Start the demo application either via `cargo run` in the `demo_app/` directory or via the compiled application. The compiled application **REQUIRES** that the templates directory is copied next to the executable.

### Packages

Packages used by application/library. For the most up-to-date information please refer to the `cargo.toml` files.

#### Weather API Library

- serde
- serde_derive
- serde_json
- reqwest
- chrono
- log
- rest_data

#### REST Data Library

- serde
- serde_json

#### Protocol Library

- serde
- serde_json
- log

#### Load Balancer Application

- rocket
- percentage
- serde
- serde_json
- serde_derive
- crypto-hash
- protocol
- rest_data
- log
- stderrlog

#### Dummy Sensor Application

- clap
- rand
- serde
- serde_derive
- serde_json
- tokio
- chrono
- weather_api
- reqwest
- log
- stderrlog
- async-std

#### Demo Application

- serde
- serde_derive
- weather_api
- rocket
- chrono

#### Database Application

- tokio
- tokio-util
- futures
- http
- serde
- serde_derive
- serde_json
- httparse
- time
- tokio-core
- tokio-io
- serde_any
- protocol
- log
- stderrlog

### Config Library

- serde
- serde_json
- serde_derive
- log

## Contributions

### Johannes Blau (blauj)

- Concept & Planning
- Demo Application (Base implementation)
- Config Library (Base implementation)
- Weather API (Base implementation)
- Dummy Sensor (Bug fixes, enhancements)
- Load Balancer (Bug fixes, enhancements)

### Lukas Grünwald (gruenwald)

- Concept & Planning
- Protocol Library (Base implementation)
- Rest Data Library (Base implementation)
- Load Balancer (Bug fixes, enhancements)
- Database (Bug fixes, enhancements)
- Dummy Sensor (Bug fixes, enhancements)

### Michael Hufnagel (hufnagel)

- Concept & Planning
- Load Balancer (Core implementation)

### Karlheinz Reinhardt (reinhardt)

- Concept & Planning
- Dummy Sensor (Base implementation)
- Weather API (Base implementation)
- Load Balancer (Bug fixes, enhancements)

### Manuel Zierl (zierl, ru76sin)

- Concept & Planning
- Database (Base implementation)
- Dummy Sensor (Bug fixes, enhancements)
