use std::error::Error;
use std::fmt;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

/// The cargo package name
pub const APP_NAME: &str = env!("CARGO_PKG_NAME");
/// The application version
pub const APP_VERSION: &str = env!("CARGO_PKG_VERSION");

/// Error type for creating configs.
#[derive(Default, Debug)]
pub struct ConfigError {
    /// Returns the error message.
    pub(self) message: String,
}

impl ConfigError {
    /// Creates a new ConfigError with the supplied message.
    pub fn new(message: String) -> Self {
        Self { message }
    }
    /// Returns the error message
    pub fn message(&self) -> &String {
        &self.message
    }
}

impl fmt::Display for ConfigError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Configuration Error: {}", self.message)
    }
}

impl Error for ConfigError {
    fn description(&self) -> &str {
        &self.message
    }
}

/// A generic trait that can be implemented by configuration objects.
/// It allows simple deserialization of the json config files.
/// The config type must implement the Serializable trait from serde.
pub trait Config<T>
where
    T: serde::de::DeserializeOwned,
{
    /// Creates a new configuration object from the file provided
    fn new_from_file(path: &str) -> Result<T, ConfigError> {
        println!("Opening config file at {}", &path);
        let fpath = Path::new(path);
        let mut file = match File::open(&fpath) {
            Ok(f) => f,
            Err(e) => return Err(ConfigError::new(e.to_string())),
        };
        let mut buf: String = String::new();
        match file.read_to_string(&mut buf) {
            Ok(_) => {}
            Err(e) => return Err(ConfigError::new(e.to_string())),
        }
        match serde_json::from_str(&buf) {
            Ok(config) => Ok(config),
            Err(e) => Err(ConfigError::new(e.to_string())),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde::Deserialize;
    use serde::Serialize;

    #[derive(Serialize, Deserialize, Debug)]
    struct TestConfig {
        my_string: String,
        my_int: u32,
    }

    impl Default for TestConfig {
        fn default() -> Self {
            Self {
                my_string: String::from("my_default"),
                my_int: 42,
            }
        }
    }

    impl Config<TestConfig> for TestConfig {}

    impl TestConfig {
        pub fn new(app_name: &str) -> Result<TestConfig, ConfigError> {
            let path = format!("{}{}", app_name, ".json");
            println!("Opening logfile at {}", &path);
            Self::new_from_file(&path)
        }

        pub fn my_string(&self) -> &String {
            &self.my_string
        }

        pub fn my_int(&self) -> u32 {
            self.my_int
        }
    }

    #[test]
    fn config_from_file() {
        let config = TestConfig::new("test_config").unwrap_or_default();
        assert_eq!(config.my_string(), "def_string");
        assert_eq!(config.my_int(), 1234);
    }
}
