use std::error::Error;
use std::fmt;

/// Defines the kind of database error.
#[derive(Debug)]
pub enum DatabaseErrorKind {
    /// When a value could not be deleted from the database.
    ValueNotDeleted,
    /// When a value could not be saved to the database.
    ValueNotSaved,
    /// When a key could not be found in the databse.
    KeyNotFound,
    /// When an error occured while accessing the database.
    InternalError,
}

impl Default for DatabaseErrorKind {
    fn default() -> Self {
        DatabaseErrorKind::InternalError
    }
}

/// Thrown when an error in the database occured.
#[derive(Debug)]
pub struct DatabaseError {
    /// Type of error.
    kind: DatabaseErrorKind,
    /// Related Key.
    key: String,
    /// Detailed error message.
    message: String,
}

impl DatabaseError {
    /// Creates new Database error.
    /// 
    /// # Arguments
    /// * `kind` - The kind of database error.
    /// * `message` - The database error message.
    pub fn new(kind: DatabaseErrorKind, key: String, message: String) -> DatabaseError {
        DatabaseError { kind, key, message }
    }
    /// Returns error kind.
    pub fn kind(&self) -> &DatabaseErrorKind {
        &self.kind
    }
    /// Returns key related to error
    pub fn key(&self) -> &String {
        &self.key
    }

    /// Returns detailed error message
    pub fn message(&self) -> &String {
        &self.message
    }
}

impl fmt::Display for DatabaseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Internal Server Error: {}", self.message)
    }
}

impl Error for DatabaseError {
    fn description(&self) -> &str {
        &self.message
    }
}
