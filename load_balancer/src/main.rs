// https://rocket.rs/v0.4/guide/getting-started/
// Nightly version of rust is required by rocket
// rustup default nightly
#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

use crate::config::LoadBalancerConfig;
use crate::db_registration_handler::DBRegistrationHandler;
#[allow(unused_imports)]
use log::{error, info};
use std::sync::{Arc, Mutex};
use std::thread;

/// The configuration for the load-balancer
mod config;
/// Database Client information
mod db_client_information;
/// Database connector
mod db_connector;
/// Database errors
mod db_error;
/// Database Registration Handlers
mod db_registration_handler;
/// Database selector
mod db_selector;
/// Definition of the public rest-interface of the database.
pub mod rest_interface;

/// Main function of the load balancer
fn main() {
    // Loads config for load balancer.
    let config = match LoadBalancerConfig::new(env!("CARGO_PKG_NAME")) {
        Ok(cfg) => cfg,
        Err(e) => {
            println!("An error occurred while parsing the configuration file.");
            println!("Error {}", e);
            println!("Aborting due to errors. Program will be closed.");
            return;
        }
    };
    stderrlog::new()
        .module(module_path!())
        .quiet(!config.log_enabled())
        .verbosity(config.log_level())
        .timestamp(stderrlog::Timestamp::Millisecond)
        .init()
        .unwrap_or_default();

    // Initialize collection to hold registered db clients.
    let registered_db_clients = Arc::new(Mutex::new(vec![]));
    let db_registration_handler = DBRegistrationHandler {
        registered_db_clients: Arc::clone(&registered_db_clients),
    };

    // Listen for db client registrations on another thread.
    let listener_ip = config.db_listener_ip().to_string();
    let listener_port = config.db_listener_port().to_string();
    let _handle = thread::spawn({
        move || {
            db_registration_handler
                .handle_db_client_registration(&listener_ip.clone(), &listener_port.clone())
        }
    });
    // Launch rest interface.
    rest_interface::rocket_launch(registered_db_clients);
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::db_connector::buffer_to_string;
    use protocol::message;
    use std::io::{Read, Write};
    use std::net::TcpStream;
    use std::time::Duration;

    const LISTENER_IP: &str = "localhost";
    const LISTENER_PORT: &str = "8889";

    #[test]
    #[ignore] // FIXME [lgr] Test currently not usable apart from debugging as it does not shut down.
    fn register_db_requests() {
        stderrlog::new()
            .module(module_path!())
            .quiet(false)
            .verbosity(6)
            .timestamp(stderrlog::Timestamp::Millisecond)
            .init()
            .unwrap();
        let registered_db_clients = Arc::new(Mutex::new(vec![]));
        let db_registration_handler = DBRegistrationHandler {
            registered_db_clients: Arc::clone(&registered_db_clients),
        };

        let child = thread::spawn({
            move || {
                db_registration_handler.handle_db_client_registration(LISTENER_IP, LISTENER_PORT)
            }
        });
        register_db_request("127.0.0.1", "8888");
        register_db_request("127.0.0.1", "1334");
        register_db_request("127.0.0.1", "3334");
        assert!(!child.join().is_err())
    }

    fn register_db_request(ip: &'static str, port: &'static str) {
        let child = thread::spawn(move || {
            thread::sleep(Duration::new(5, 0));
            info!("Sending test message...");
            let server_address = format!("{}:{}", LISTENER_IP, LISTENER_PORT);
            let mut stream = match TcpStream::connect(&server_address) {
                Ok(str) => {
                    info!("Connection established.");
                    str
                }
                Err(e) => {
                    panic!("{:?}", e);
                }
            };
            let req = message::get_request_message()
                .with_sender("127.0.0.1:12345".to_string())
                .with_host_address(ip.to_string())
                .with_port(port.to_string())
                .get_register_db();
            let req_string = match serde_json::to_string(&req) {
                Ok(r) => r,
                Err(e) => {
                    error!("{:?}", e);
                    String::from("ERROR")
                }
            };
            assert_ne!("ERROR", &req_string);
            info!("Try write: {}", req_string);
            match stream.write(req_string.as_bytes()) {
                Err(e) => panic!("{:?}", e),
                Ok(_) => info!("Write success."),
            };

            let mut buffer = [0 as u8; 2048];
            match stream.read(&mut buffer) {
                Ok(_) => {
                    let message = buffer_to_string(buffer.to_vec());
                    info!("Message received {}.", message);
                    let msg: protocol::message::Message =
                        serde_json::from_str(message.as_str()).unwrap_or_default();
                    assert_eq!(
                        protocol::message::MessageType::ResponseRegisterDatabase,
                        msg.msg_type()
                    );
                }
                Err(e) => {
                    error!(
                        "test_register_db_request: Error while waiting for answer: {}",
                        e
                    );
                }
            }
        });
        assert!(!child.join().is_err())
    }
}
