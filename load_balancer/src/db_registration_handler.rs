use crate::db_client_information::DBClientInformation;
use crate::db_connector;
use log::*;
use protocol::message::response::Status;
use protocol::message::response::Status::{Failure, Success};
use protocol::message::MessageType::RequestRegisterDatabase;
use protocol::message::{get_response_message, request, Message};
use std::io::Write;
use std::net::{SocketAddr, TcpListener, TcpStream};
use std::sync::{Arc, Mutex};

/// Handles db client registration and saves registered db clients.
pub struct DBRegistrationHandler {
    /// Clients that are registed to load balancer
    pub registered_db_clients: Arc<Mutex<Vec<DBClientInformation>>>,
}

impl DBRegistrationHandler {
    /// Handle client registration with a `TcpListener` that listens for requests on the `listener_ip` and `listener_port`.
    /// 
    /// # Arguments
    /// `listener_ip` - Ip on which the load balancer listens for incoming registration requests.
    /// `listener_port` - Port on which the load balancer listens for incoming registration requests.
    pub fn handle_db_client_registration(&self, listener_ip: &str, listener_port: &str) {
        let listener_adr = format!("{}:{}", listener_ip, listener_port);

        let listener_res = TcpListener::bind(&listener_adr);

        let listener = match listener_res {
            Ok(listener) => {
                info!("Created listener {}.", &listener_adr);
                listener
            }
            Err(e) => {
                error!(
                    "Could not create tcp listener on {} for registering db clients. Error: {:?}",
                    &listener_adr, e
                );
                return;
            }
        };

        loop {
            match listener.accept() {
                Ok((stream, db_client_adr)) => {
                    self.handle_db_client_request(&listener_adr, stream, db_client_adr)
                }
                Err(e) => error!("couldn't get client: {:?}", e),
            }
        }
    }

    /// Handle the requests received on the `stream` of type `TcpStream`.
    fn handle_db_client_request(
        &self,
        listener_adr: &str,
        mut stream: TcpStream,
        db_client_adr: SocketAddr,
    ) {
        info!("handle db client: {:?}", db_client_adr);
        let message_string = match db_connector::wait_for_message(&stream) {
            Ok(message_string) => message_string,
            Err(e) => {
                error!(
                    "Error while waiting for response of client: {:?} Error: {:?}",
                    db_client_adr, e
                );
                String::from("")
            }
        };

        if message_string.is_empty() {
            return;
        }

        let message = serde_json::from_str::<Message>(message_string.as_str()).unwrap_or_default();
        match message.msg_type() {
            RequestRegisterDatabase =>
                {
                    let response = self.get_register_database_response(&listener_adr, &message);

                    match stream.write(serde_json::to_string(&response).unwrap_or_default().as_bytes()) {
                        Ok(number_of_bytes) => {
                            debug!("Write successful. Wrote {} bytes to stream.", number_of_bytes);
                        }
                        Err(e) => {
                            error!("Error while waiting for response of client: {:?} Error: {:?}", db_client_adr, e);
                        }
                    };
                }
            message_type => warn!("Wrong message type received. Message type is not RequestRegisterDatabase but {:?}.", message_type),
        }
    }

    /// Gets the response to the provided `message`.
    fn get_register_database_response(&self, listener_adr: &str, message: &Message) -> Message {
        let request = request::deserialize_register_database(message.content());
        let response_status = self.handle_register_database_request(request);

        get_response_message()
            .with_sender(String::from(listener_adr))
            .with_status(response_status)
            .register_db()
    }

    /// Add the port and address contained in the `request` of type `request::RegisterDatabase` to the `self.registered_db_clients`,
    /// if the request was valid and if a db client with the same information is not yet registered.
    fn handle_register_database_request(&self, request: request::RegisterDatabase) -> Status {
        if request.is_valid() {
            let db_client_port = request.port();
            let db_client_host_address = request.host_address();
            let mut db_clients = match self.registered_db_clients.lock() {
                Ok(c) => {
                    debug!("DB clients locked.");
                    c
                }
                Err(e) => {
                    error!("DB clients could not be locked: {}", e);
                    return Failure;
                }
            };

            let db_client_info = DBClientInformation {
                address: db_client_host_address.clone(),
                port: db_client_port.clone(),
            };
            if !db_clients.contains(&db_client_info) {
                db_clients.push(DBClientInformation {
                    address: db_client_host_address.clone(),
                    port: db_client_port.clone(),
                });
            } else {
                info!(
                    "db_client with information {:?} already exists.",
                    db_client_info
                );
            }

            info!("db_clients: {:?}", db_clients);
            Success
        } else {
            error!("Received invalid request to register Database.");
            Failure
        }
    }
}
