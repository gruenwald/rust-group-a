use config::{Config, ConfigError};
use serde::Deserialize;
use serde::Serialize;

/// The default log level: 6.
const DEFAULT_LOG_LEVEL: usize = 6;
/// True, if logging is enabled on default.
const DEFAULT_LOG_ENABLED: bool = false;
/// The default listener address.
/// Use localhost or 127.0.0.1 if 0.0.0.0 is slow on your machine.
const DEFAULT_DB_LISTENER_IP: &str = "0.0.0.0";
/// The default listener port for incoming database connections.
const DEFAULT_DB_LISTENER_PORT: &str = "8889";
/// The default listener address for the rest api.
/// Use localhost or 127.0.0.1 if 0.0.0.0 is slow on your machine.
const DEFAULT_API_ADDRESS: &str = "0.0.0.0";
/// The default port for the rest api.
const DEFAULT_API_PORT: u16 = 8000;

/// Wrapper for the default database listener ip.
/// Used for serde default.
pub fn default_db_listener_ip() -> String {
    String::from(DEFAULT_DB_LISTENER_IP)
}

/// Wrapper for the default database listener port.
/// Used for serde default.
pub fn default_api_listener_ip() -> String {
    String::from(DEFAULT_DB_LISTENER_IP)
}

/// Holds configruation information for starting the rest_interface and for listening for db clients that want to register.
#[derive(Serialize, Deserialize, Debug)]
pub struct LoadBalancerConfig {
    /// Log level of load balancer.
    log_level: usize,
    /// Log level activation.
    log_enabled: bool,
    /// The ip to which the database clients will register.
    #[serde(skip, default = "default_db_listener_ip")]
    db_listener_ip: String,
    /// The port to which the database clients will register.
    db_listener_port: String,
    /// The ip of the REST-Interface.
    api_address: String,
    /// The port of the REST-Interface.
    api_port: u16,
}

impl Default for LoadBalancerConfig {
    fn default() -> Self {
        Self {
            log_level: DEFAULT_LOG_LEVEL,
            log_enabled: DEFAULT_LOG_ENABLED,
            db_listener_ip: String::from(DEFAULT_DB_LISTENER_IP),
            db_listener_port: String::from(DEFAULT_DB_LISTENER_PORT),
            api_address: String::from(DEFAULT_API_ADDRESS),
            api_port: DEFAULT_API_PORT,
        }
    }
}

impl Config<LoadBalancerConfig> for LoadBalancerConfig {}

impl LoadBalancerConfig {
    /// Creates a new load-balancer config from a configuration file.
    /// 
    /// # Arguments
    /// * `app_name` - The name of the running application. Is used for the filename.
    pub fn new(app_name: &str) -> Result<LoadBalancerConfig, ConfigError> {
        let path = format!("{}{}", app_name, ".json");
        println!("Opening logfile at {}", &path);
        Self::new_from_file(&path)
    }
    /// Returns the log level.
    pub fn log_level(&self) -> usize {
        self.log_level
    }
    /// Returns if logging is enabled.
    pub fn log_enabled(&self) -> bool {
        self.log_enabled
    }
    /// Returns the database listener ip.
    pub fn db_listener_ip(&self) -> &String {
        &self.db_listener_ip
    }
    /// Returns the database listener port.
    pub fn db_listener_port(&self) -> &String {
        &self.db_listener_port
    }
    /// Returns the rest-api address.
    pub fn api_address(&self) -> &String {
        &self.api_address
    }
    /// Returns the rest-api port.
    pub fn api_port(&self) -> u16 {
        self.api_port
    }
}
