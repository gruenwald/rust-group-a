use crate::db_client_information::DBClientInformation;
use crate::db_error::{DatabaseError, DatabaseErrorKind};
use crate::db_selector;
use log::*;
use protocol;
use protocol::message;
use protocol::message::response;
use protocol::message::response::Status::Success;
use protocol::message::MessageType::ResponseGetByPartialKey;
use protocol::message::{get_request_message, Message};
use std::error::Error;
use std::io::{BufRead, BufReader, Read, Write};
use std::net::TcpStream;
use std::result::Result;
use std::sync::{Arc, Mutex};

/// Used for sending get, add and delete requests to the db clients.
pub struct DBConnector {
    /// Ip Address and port of rest interface.
    pub rest_interface_address: String,
    /// Db clients registered at load balancer.
    pub registered_db_clients: Arc<Mutex<Vec<DBClientInformation>>>,
}

impl DBConnector {
    /// Get a value by its fully qualified key i.e. `exact_key`.
    /// 
    /// # Arguments
    /// * `exact_key` - Fully qualified key to get one value.
    pub fn get_value_by_fully_qualified_key(
        &self,
        exact_key: String,
    ) -> Result<String, DatabaseError> {
        let req = get_request_message()
            .with_sender(self.rest_interface_address.clone())
            .with_key(exact_key.clone())
            .get_by_fully_qualified_key();
        let req_string = serde_json::to_string(&req).unwrap_or_default();

        let response_string = self.try_select_db_client_and_send(&exact_key, req_string, 0);
        match response_string {
            Ok(msg_str) => Ok(msg_str),
            Err(e) => Err(DatabaseError::new(
                DatabaseErrorKind::KeyNotFound,
                exact_key,
                e.to_string(),
            )),
        }
    }

    /// Search every db client for data associated to a partial key. Concatenate the responses and return them.
    /// 
    /// # Arguments
    /// * `key` - Partial key to get multiple values.
    pub fn get_by_partial_key(&self, key: String) -> Result<String, DatabaseError> {
        let req = get_request_message()
            .with_sender(self.rest_interface_address.clone())
            .with_key(key.clone())
            .get_by_partial_key();
        let req_string = serde_json::to_string(&req).unwrap_or_default();

        let registered_db_clients = self.get_registered_db_clients_clone();
        let mut responses: Vec<(String, String)> = vec![];
        // send a request for the partial key to every registered db_client
        for registered_db_client in registered_db_clients {
            info!("Try {}", &registered_db_client);
            let response = self.connect_and_write_to_db(&registered_db_client, &req_string);
            match response {
                Ok(message_string) => {
                    let message = serde_json::from_str::<Message>(message_string.as_str())
                        .unwrap_or_default();
                    if message.msg_type() == ResponseGetByPartialKey {
                        let response: response::GetByPartialKey =
                            response::deserialize_get_by_partial_key(message.content());
                        if response.is_valid() && response.status() == Success {
                            let mut des_response: Vec<(String, String)> =
                                serde_json::from_str(response.value())
                                    .expect("Error deserializing response value.");
                            responses.append(&mut des_response);
                        }
                    }
                }
                Err(e) => {
                    error!(
                        "Error requesting Key {} in registered db client {:?}. Error {:?}",
                        key, registered_db_client, e
                    );
                }
            }
        }

        if !responses.is_empty() {
            match serde_json::to_string(&responses) {
                Ok(responses) => {
                    info!("responses: {:?}", responses);
                    return Ok(responses);
                }
                Err(e) => {
                    error!(
                        "Could not parse responses for get by partial key request. Error: {:?}",
                        e
                    );
                }
            }
        }

        Err(DatabaseError::new(
            DatabaseErrorKind::KeyNotFound,
            key,
            "Partial Key Not Found".to_string(),
        ))
    }

    /// Add a `value` with a fully qualified key i.e. `exact_key` to the database.
    /// 
    /// # Arguments
    /// * `exact_key` - Fully qualified key to get one value.
    /// * `value` - Value to insert into database.
    pub fn add(&self, exact_key: String, value: &String) -> Result<String, DatabaseError> {
        let req = get_request_message()
            .with_sender(self.rest_interface_address.clone())
            .with_key(exact_key.clone())
            .with_value(value.clone())
            .insert();
        let req_string = serde_json::to_string(&req).unwrap_or_default();
        self.select_db_client_and_send(&exact_key, &req_string)
            .map_err(|e| {
                DatabaseError::new(DatabaseErrorKind::ValueNotSaved, exact_key, e.to_string())
            })
    }

    /// Delete a `value` with a fully qualified key i.e. `exact_key` from the database.
    /// 
    /// # Arguments
    /// * `exact_key` - the exacte key leading to the node you want to delete
    pub fn delete(&self, exact_key: String) -> Result<String, DatabaseError> {
        let req = get_request_message()
            .with_sender(self.rest_interface_address.clone())
            .with_key(exact_key.clone())
            .clear();
        let req_string = serde_json::to_string(&req).unwrap_or_default();

        self.select_db_client_and_send(&exact_key, &req_string)
            .map_err(|e| {
                DatabaseError::new(DatabaseErrorKind::ValueNotDeleted, exact_key, e.to_string())
            })
    }

    /// Delete an `exact_key` from a specific db client, with the declared `db_client_information`.
    /// 
    /// # Arguments
    /// * `exact_key` - the exacte key leading to the node you want to delete
    /// * `db_client_information` - info about db-client
    pub fn delete_from_specific_db_client(
        &self,
        exact_key: String,
        db_client_information: DBClientInformation,
    ) -> Result<String, DatabaseError> {
        let req = get_request_message()
            .with_sender(self.rest_interface_address.clone())
            .with_key(exact_key.clone())
            .clear();
        let req_string = serde_json::to_string(&req).unwrap_or_default();

        self.connect_and_write_to_db(&db_client_information, &req_string)
            .map_err(|e| {
                DatabaseError::new(DatabaseErrorKind::ValueNotDeleted, exact_key, e.to_string())
            })
    }

    /// Get the value of an `exact_key` request or an error depending on the `message::response::Status` in the `response_string`.
    fn get_value_from_response_string(
        &self,
        response_string: &str,
        exact_key: &str,
    ) -> Result<String, DatabaseError> {
        let msg = serde_json::from_str::<message::Message>(response_string).unwrap_or_default();
        let response = message::response::deserialize_get_by_fully_qualified_key(&msg.content());
        match response.status() {
            message::response::Status::Success => Ok(response.value().clone()),
            message::response::Status::Failure => {
                let error_msg = format!("Key '{}' not found", &exact_key);
                Err(DatabaseError::new(
                    DatabaseErrorKind::KeyNotFound,
                    exact_key.to_string(),
                    error_msg,
                ))
            }
            _ => Err(DatabaseError::new(
                DatabaseErrorKind::KeyNotFound,
                exact_key.to_string(),
                response.value().clone(),
            )),
        }
    }

    /// Try requesting a value for an exact key. If the request fails, increment the number of retries and call this function again.
    /// If the request succeeds return the requested value. Additionally try to re-balance the value into another db client if the number of
    /// retries is larger than one.
    /// If there are no db clients to select anymore return an error.
    fn try_select_db_client_and_send(
        &self,
        exact_key: &String,
        request_string: String,
        mut number_of_retries: usize,
    ) -> Result<String, Box<dyn Error>> {
        match db_selector::select_db_client(
            self.registered_db_clients.clone(),
            String::from(exact_key),
            number_of_retries,
        ) {
            Ok(selected_db_client_info) => {
                info!(
                    "Requesting value with exact_key {} from {:?}. Retry number {}.",
                    exact_key, selected_db_client_info, number_of_retries
                );
                if let Ok(response) =
                    self.connect_and_write_to_db(&selected_db_client_info, &request_string)
                {
                    if let Ok(response_string) =
                        self.get_value_from_response_string(&response, &exact_key)
                    {
                        self.try_rebalance(
                            exact_key,
                            &response_string,
                            number_of_retries,
                            selected_db_client_info,
                        );
                        return Ok(response_string);
                    }
                }
                //try again with increased counter if connection to the db client or request sent to the db client or parsing of the response failed
                number_of_retries += 1;
                self.try_select_db_client_and_send(exact_key, request_string, number_of_retries)
            }
            Err(e) => Err(e),
        }
    }

    /// Try to move a key value pair, if a request was retried.
    fn try_rebalance(
        &self,
        exact_key: &String,
        value: &String,
        number_of_retries: usize,
        selected_db_client_info: DBClientInformation,
    ) {
        if number_of_retries != 0 && !self.add(exact_key.clone(), value).is_err() {
            if self
                .delete_from_specific_db_client(exact_key.clone(), selected_db_client_info)
                .is_err()
            {
                warn!("Failed to delete exact_key: {}", exact_key)
            }
            info!("Rebalanced successfully.");
        }
    }

    /// Select a db client based on the `exact_key` and send a provided `req_string`.
    fn select_db_client_and_send(
        &self,
        exact_key: &str,
        req_string: &str,
    ) -> Result<String, Box<dyn Error>> {
        match db_selector::select_db_client(
            self.registered_db_clients.clone(),
            String::from(exact_key),
            0,
        ) {
            Ok(selected_db_client_info) => {
                self.connect_and_write_to_db(&selected_db_client_info, &req_string)
            }
            Err(e) => {
                error!("Error while selecting db client. Error: {:?}", e);
                Err(e)
            }
        }
    }

    /// Connect to db client with `db_client_information` and write the provided `request`.
    fn connect_and_write_to_db(
        &self,
        db_client_information: &DBClientInformation,
        request: &str,
    ) -> Result<String, Box<dyn Error>> {
        let stream = connect(format!(
            "{}:{}",
            db_client_information.address, db_client_information.port
        ))?;
        write_to_db(stream, String::from(request))
    }

    /// Get a clone from the collection of registered db clients.
    fn get_registered_db_clients_clone(&self) -> Vec<DBClientInformation> {
        let mut registered_db_clients: Vec<DBClientInformation> = vec![];

        match self.registered_db_clients.clone().lock() {
            Ok(registered_db_clients_guard) => {
                registered_db_clients = registered_db_clients_guard.clone();
            }
            Err(e) => {
                error!(
                    "Failed to get lock for registered_db_clients. Error: {:?}",
                    e
                );
            }
        }

        registered_db_clients
    }
}

/// Parse `buffer` to `String`.
/// 
/// # Arguments
/// * `buffer` - the buffer
pub fn buffer_to_string(buffer: Vec<u8>) -> String {
    let msg_str = match String::from_utf8(buffer) {
        Ok(msg) => msg,
        Err(e) => {
            error!("Could not parse message to utf8. Error: {:?}", e);
            String::from("")
        }
    };
    String::from(msg_str.trim_matches(char::from(0)))
}

/// Write the `request` to the `stream`.
fn write_to_db(mut stream: TcpStream, request: String) -> Result<String, Box<dyn Error>> {
    let request_as_bytes = request.as_bytes();
    match stream.write_all(request_as_bytes) {
        Ok(_) => debug!("Message written successfully: {}", request),
        Err(e) => {
            error!("An error occurred: {}", e);
            return Err(Box::new(e));
        }
    };
    wait_for_message(&stream)
}

/// Wait for a message on the `stream`
/// 
/// # Arguments
/// * `stream` - the Tcp Stream where is waited for message
pub(crate) fn wait_for_message(stream: &TcpStream) -> Result<String, Box<dyn Error>> {
    let mut reader = BufReader::new(stream);
    let mut string_buf = String::new();
    for line in reader.by_ref().lines() {
        let l = match line {
            Ok(c) => {
                info!("Received line {}", c);
                c
            }
            Err(e) => {
                error!("An error occurred during reading the line {}", e);
                return Err(Box::new(e));
            }
        };
        string_buf.push_str(&l);
    }
    Ok(string_buf)
}

/// Connect to the provided `address`. Get a `TcpStream` if the connection was successful or an `dyn Error` if not.
fn connect(address: String) -> Result<TcpStream, Box<dyn Error>> {
    match TcpStream::connect(&address) {
        Ok(stream) => {
            info!("Connected to {}", address);
            Ok(stream)
        }
        Err(e) => {
            error!("Failed to connect: {:?}", e);
            Err(Box::new(e))
        }
    }
}
