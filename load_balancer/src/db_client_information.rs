use std::fmt;

/// Information of a db client. Db client `address` and `port`.
#[derive(Debug, Clone, PartialEq)]
pub struct DBClientInformation {
    /// Ip Address of a db client.
    pub address: String,
    /// Port of a db client.
    pub port: String,
}

impl fmt::Display for DBClientInformation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}:{}", self.address, self.port)
    }
}
