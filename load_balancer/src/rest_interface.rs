use crate::config::LoadBalancerConfig;
use crate::db_client_information::DBClientInformation;
use crate::db_connector::DBConnector;
use crate::db_error::DatabaseErrorKind;
use log::*;
use rest_data::DataWrapper;
use rocket::config::{Config, Environment, LoggingLevel};
use rocket::http::Status;
use rocket::State;
use rocket_contrib::json::Json;
use std::result::Result;
use std::sync::{Arc, Mutex};

/// Contains communication capabilities with database. Every request route can access the model.
pub struct Model {
    /// Provides communication capabilities with the database.
    db_connector: DBConnector,
}

/// # Arguments
/// * `registered_db_clients`- All registered database clients.
/// 
/// # Usage
/// Launches rocket application i.e. the rest interface.
pub fn rocket_launch(registered_db_clients: Arc<Mutex<Vec<DBClientInformation>>>) {
    info!("===== LAUNCHING ROCKET =====");
    // No harm to use the default here. If the file doesn't exist, this thread doesn't exist.
    let config = LoadBalancerConfig::new(env!("CARGO_PKG_NAME")).unwrap_or_default();
    let db_connector = DBConnector {
        rest_interface_address: format!("{}:{}", config.api_address(), config.api_port()),
        registered_db_clients,
    };
    let model = Model { db_connector };

    let rocket_config = match Config::build(Environment::Staging)
        .address(config.api_address())
        .port(config.api_port())
        .log_level(LoggingLevel::Normal)
        .finalize()
    {
        Ok(c) => {
            info!("Launching REST API at {}:{}", config.api_address(), config.api_port());
            rocket::custom(c)
        }
        Err(_e) => {
            warn!("Couldn't create rocket config, using ignite()");
            rocket::ignite()
        }
    };
    let error = rocket_config
        .manage(model)
        .mount("/", routes![get_data, save_data, broad_search])
        .launch();
    error!("An error occurred whilst launching Rocket: {}", error);
}

/// # Arguments
/// * `model` - holds communication capabilities with distributed database clients
/// * `exact_key`- key that has one value
/// 
/// # Returns
/// A Result<String, Status>`:
/// * `Ok` - The value found for the provided `exact_key`
/// * `Err` - A `Status` indicating the error when requesting the value of `exact_key`. `Status::NotFound` if the key could not be found or `Status::InternalServerError` if another error occurred while getting the value from the Key Value Database.
/// 
/// # Usage
/// Send a http get request to the address '/<exact_key>'
#[get("/<exact_key>")]
fn get_data(model: State<Model>, exact_key: String) -> Result<String, Status> {
    info!("Handling get_data request for key {}", exact_key);
    let value = model
        .db_connector
        .get_value_by_fully_qualified_key(exact_key.clone());
    match value {
        Err(e) => match e.kind() {
            DatabaseErrorKind::KeyNotFound => {
                warn!("The requested key \"{}\" does not exist.", exact_key);
                Err(Status::NotFound)
            }
            _ => {
                error!("An internal server error occurred.");
                Err(Status::InternalServerError)
            }
        },
        Ok(v) => Ok(v),
    }
}

/// # Arguments
/// * `model` - holds communication capabilities with distributed database clients
/// * `search_key` - sub key that is used for searching everything below the sub key
/// 
/// # Returns
/// A Result<String, Status>`:
/// * `Ok` - The values found for the provided `search_key`
/// * `Err` - A `Status` indicating the error when requesting the values of `search_key`. `Status::NotFound` if the key could not be found or `Status::InternalServerError` if another error occurred while getting the values from the Key Value Database.
/// 
/// # Usage
/// Send a http get request to the address '/search/<search_key>'
#[get("/search/<search_key>")]
fn broad_search(model: State<Model>, search_key: String) -> Result<String, Status> {
    info!("Handling broad_search request for key {}", search_key);
    let value = model.db_connector.get_by_partial_key(search_key.clone());
    match value {
        Err(e) => match e.kind() {
            DatabaseErrorKind::KeyNotFound => {
                warn!("No values found for key \"{}\".", search_key);
                Err(Status::NotFound)
            }

            _ => {
                error!("An internal server error occurred.");
                Err(Status::InternalServerError)
            }
        },
        Ok(v) => Ok(v),
    }
}

/// # Arguments
/// * `model` - holds communication capabilities with distributed database clients
/// * `exact_key` - key that has one value
/// * `wrapped_data` - payload that should be saved
/// 
/// # Returns
/// A Result<String, Status>`:
/// * `Ok` - The values found for the provided `search_key`
/// * `Err` - A `Status` indicating the error when trying to save a key value pair to the Key Value Database `Status::ValueNotSaved` if the key could not be saved or `Status::InternalServerError` if another error occurred while saving the key value pair to the Key Value Database.
/// 
/// # Usage
/// Send a http post request to the address '/save/<exact_key>'. Provide a body of `Json<DataWrapper>`.
#[post("/save/<exact_key>", data = "<wrapped_data>")]
fn save_data(
    model: State<Model>,
    exact_key: String,
    wrapped_data: Json<DataWrapper>,
) -> Result<String, Status> {
    info!(
        "Handling save_data request for {}:{}",
        exact_key,
        wrapped_data.body_as_str()
    );
    let result = model
        .db_connector
        .add(exact_key.clone(), &wrapped_data.body().to_string());
    match result {
        Err(e) => match e.kind() {
            DatabaseErrorKind::ValueNotSaved => {
                warn!("Could not save data for key \"{}\".", exact_key);
                Err(Status::InternalServerError)
            }

            _ => {
                error!("An internal server error occurred.");
                Err(Status::InternalServerError)
            }
        },
        Ok(v) => Ok(v),
    }
}
