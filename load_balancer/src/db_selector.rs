use crate::db_client_information::DBClientInformation;
use crypto_hash::{digest, Algorithm};
use log::*;
use std::convert::TryFrom;
use std::error::Error;
use std::sync::{Arc, Mutex};

/// Selects from `registered_db_clients` based on the hash of the `exact_key`. The range that can be selected in `registered_db_clients`,
/// depends on the `number_of_req_retries`.
/// 
/// # Arguments
/// * `registered_db_clients` - list of registered db-clients packed inside a Arc<Mutex<>>
/// * `exact_key` - the exact key for which a database should be selected
/// * `number_of_req_retries` - number of retries
pub fn select_db_client(
    registered_db_clients: Arc<Mutex<Vec<DBClientInformation>>>,
    exact_key: String,
    number_of_req_retries: usize,
) -> Result<DBClientInformation, Box<dyn Error>> {
    if let Ok(registered_db_clients) = registered_db_clients.lock() {
        // decrease the range that will be considered when calculating the index
        let registered_db_clients_len = registered_db_clients.len() - number_of_req_retries;

        if registered_db_clients_len < 1 {
            return Err(From::from("No DB client to select."));
        }

        let db_index = calculate_index_to_select(registered_db_clients_len, exact_key)?;

        match registered_db_clients.get(db_index) {
            Some(selected_db_client) => return Ok(selected_db_client.clone()),
            _ => {
                error!(
                    "db client not found in registered_db_clients collection for index {}.",
                    db_index
                );
                return Err(From::from("Could not select db client."));
            }
        }
    }

    Err(From::from("Failed to get lock for registered_db_clients."))
}

/// Hashes the `exact_key`. Takes last element of digest. Calculate modulo of digest and registered number of clients.
fn calculate_index_to_select(
    registered_db_clients_len: usize,
    exact_key: String,
) -> Result<usize, Box<dyn Error>> {
    let digest_result = digest(Algorithm::SHA256, exact_key.as_bytes());
    let parsed_length = u8::try_from(registered_db_clients_len)?;

    let digest_last_element = digest_result[digest_result.len() - 1];
    let index = digest_last_element % parsed_length;

    let db_index = usize::try_from(index)?;
    Ok(db_index)
}
