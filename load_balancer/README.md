# The Load Balancer Application

The load balancer application is the single point of access for the distributed key-value database.
For incoming requests it provides a REST API.

Communication with the database instances are handled via TCP connections.

## Configuration

The load balancer relies on an application configuration file named `load_balancer.json`.

The default configuration:

```json
{
  "log_level": 3,
  "log_enabled": true,
  "db_listener_port": "8889",
  "api_port": "8000"
}
```

### The `log_level` Parameter

The `log_level` determines the granularity of the log output, with 1 being the lowest log level.
See the [log crate](https://docs.rs/log/0.4.10/log/) for more details.

### The `log_enabled` Parameter

Enables or disables logging completely.

### The `db_listener_port` Parameter

Determines on which port the application listens for new DB registration requests.

### The `api_port` Parameter

Determines on which port the REST API can be reached.

## REST API

### Key Structure

The Database is structured like a tree. Be aware that key and subkey are separated by dots (e.g. `key1.key1_1.key1_1_3`) for insert operations as well as search requests, so the the key itself should not contain any dots.

### Get By Fully Qualified Key

`https://balancer-host:api_port/key`

Queries the database(-s) for a given key.  
Returns a value if found, an error (`Status::NotFound`) if not.

### Broad Search

`https://balancer-host:api_port/search/partial-key`

Queries the database(-s) for a given partial key.  
Returns a list of values if found, an error (`Status::NotFound`) if not.

### Save Key-Value Pair

`https://balancer-host:api_port/save/key`

Saves the body of an http request under the given key.  
Returns a `Status::InternalServerError` if the save fails.
