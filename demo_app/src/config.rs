use config::{Config, ConfigError};
use serde::{Deserialize, Serialize};

/// Holds Demo Application configuration.
#[derive(Serialize, Deserialize, Debug)]
pub struct DemoApplicationConfig {
    /// Port that the demo app will listen on new requests.
    app_listener_port: u16,
    /// Ip of the load balancer.
    load_balancer_ip: String,
    /// Port of the load balancer.
    load_balancer_port: u16,
}

impl Default for DemoApplicationConfig {
    fn default() -> Self {
        Self {
            app_listener_port: 8887,
            load_balancer_ip: "127.0.0.1".to_string(),
            load_balancer_port: 8000,
        }
    }
}

impl Config<DemoApplicationConfig> for DemoApplicationConfig {}

impl DemoApplicationConfig {
    /// Creates a new configuration for the demo app
    /// # Arguments
    /// * `app_name` - The name of the .json config file without the extension
    pub fn new(app_name: &str) -> Result<DemoApplicationConfig, ConfigError> {
        let path = format!("{}{}", app_name, ".json");
        Self::new_from_file(&path)
    }
    /// Returns the port on which the web app listens
    pub fn app_listener_port(&self) -> u16 {
        self.app_listener_port
    }
    /// Returns the ip of the load balancer
    pub fn load_balancer_ip(&self) -> &String {
        &self.load_balancer_ip
    }
    //Returns the port of the load balancer
    pub fn load_balancer_port(&self) -> u16 {
        self.load_balancer_port
    }
}
