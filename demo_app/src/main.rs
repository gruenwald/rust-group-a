#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use]
extern crate rocket;
/// Demo app configuration
mod config;

use crate::config::DemoApplicationConfig;
use chrono::{Date, Timelike, Utc};
use log::{error, info, warn};
use rocket::config::{Config, Environment, LoggingLevel};
use rocket::{Request, State};
use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::Template;
use serde_derive::Serialize;
use std::collections::HashMap;

use weather_api::sync_weather_provider as provider;
use weather_api::weather_rest_api as api;

fn main() {
    stderrlog::new()
        .module(module_path!())
        .quiet(false)
        .verbosity(6)
        .timestamp(stderrlog::Timestamp::Millisecond)
        .init()
        .unwrap_or_default();

    let config = match DemoApplicationConfig::new(env!("CARGO_PKG_NAME")) {
        Ok(cfg) => cfg,
        Err(e) => {
            warn!("An error occurred while parsing the configuration file.");
            warn!("Error {}", e);
            warn!("Using default config");
            DemoApplicationConfig::default()
        }
    };
    info!("Balancer IP: {}", config.load_balancer_ip());

    let rocket_config = match Config::build(Environment::Staging)
        .address("0.0.0.0")
        .port(config.app_listener_port())
        .log_level(LoggingLevel::Debug)
        .finalize()
    {
        Ok(c) => rocket::custom(c),
        Err(_e) => {
            warn!("Couldn't create rocket config, using ignite()");
            rocket::ignite()
        }
    };

    let error = rocket_config
        .mount("/", routes![root, sensors, measurements])
        .mount("/public", StaticFiles::from("templates/public"))
        .manage(config)
        .register(catchers![not_found])
        .attach(Template::fairing())
        .launch();
    error!("An error occurred whilst launching Rocket: {}", error);
}

#[catch(404)]
/// Show a pretty error page when a 404 happens
fn not_found(_req: &Request) -> Template {
    let context = HashMap::<String, String>::new();
    Template::render("404", context)
}

#[derive(Serialize)]
/// Holds the context for the Index page template.
struct IndexTemplateContext<'a> {
    /// Contains a mapping from locationName -> weather data
    measures: HashMap<&'a str, api::WeatherDataMessage>,
}

#[derive(Serialize)]
/// Holds the context for the measurements page.
struct MeasurementsTemplateContext {
    /// The name of the location, e.g. Munich.
    location: String,
    /// The date of the shown measurement.
    date: String,
    /// Is true if there is no data for the requested day.
    not_found: bool,
    /// A list of the temperatures ordered by the hour.
    /// The index corresponds to the hour at which it was registered.
    measures: [i32; 24],
}

#[get("/")]
/// Root path of the web app.
/// Retrieves the latest weather data from the 4 shown locations and displays them
/// # Arguments
/// * `config` - Implicit Rocket state.
fn root(config: State<DemoApplicationConfig>) -> Template {
    let mut measures = HashMap::<&str, api::WeatherDataMessage>::new();
    let provider = provider::SyncWeatherProvider::new(
        config.load_balancer_ip(),
        config.load_balancer_port() as u32,
    );
    let iter = weather_api::LOCATIONS.iter();
    for loc in iter {
        if let Ok(data) = provider.get_latest_weather_data(&loc, api::SensorType::Temperature) {
            info!("Loc: {}", &loc);
            info!("Data: {}", &data.location);
            measures.insert(&loc, data);
        }
    }
    let context = IndexTemplateContext { measures };
    Template::render("index", context)
}

#[get("/sensors")]
fn sensors() -> Template {
    let context = HashMap::<String, String>::new();
    Template::render("sensors", context)
}

#[get("/sensors/<location>")]
/// Shows the measurements of the current day from a given location
/// Retrieves the data via a subkey search <Location>.Temperature and sorts them by time
/// The resulting array of temperatures is provided as context for the chart
/// # Arguments
/// * `config` - Implicit Rocket state.
/// * `location` - Sensor Location.
fn measurements(config: State<DemoApplicationConfig>, location: String) -> Template {
    let provider = provider::SyncWeatherProvider::new(
        config.load_balancer_ip(),
        config.load_balancer_port() as u32,
    );
    let mut not_found = false;
    let today: Date<Utc> = Utc::today();
    let mut measures: [i32; 24] = [99; 24];
    if let Ok(data) =
        provider.get_weather_data_by_date(location.as_str(), api::SensorType::Temperature, today)
    {
        for item in data.iter() {
            if let Some(tmp) = item.temperature {
                let hour = item.date_time.hour() as usize;
                if hour < 24 {
                    measures[hour] = tmp;
                }
            }
        }
    } else {
        not_found = true;
    }

    let context = MeasurementsTemplateContext {
        location,
        date: today.to_string(),
        not_found,
        measures,
    };
    Template::render("sensor", context)
}
