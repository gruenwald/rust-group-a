# The Demo Application

The demo application is a proof of concept application that shows the K-V-database's capabilities.

It uses the load balancer's provided REST API for all operations.

For a standalone application, it is **REQUIRED** that the templates directory is copied next to the executable.

## Configuration

The demo application relies on an application configuration file named `demo_app.json` next to the executable.

The default configuration:

```json
{
    "app_listener_port": "8887",
    "load_balancer_ip": "127.0.0.1",
    "load_balancer_port": "8000"
}
```

### The `app_listener_port` Parameter

The port on the local host that the app can be reached under.

### The `load_balancer_ip` Parameter

The IP of the machine running the load balancer.

### The `load_balancer_port` Parameter

The port of the load balancer's REST API.
