use log::*;
use protocol;
use protocol::message;
use std::collections::HashMap;
use tokio;
use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;

#[derive(Default, Debug)]
/// The Database objects can represent every Node of the Tree like structure
pub struct Database {
    /// All the children of thi Database node packed in a hashmap. Must we unwraped because children can also be None (for leaf nodes) 
    pub children: Option<HashMap<String, Database>>,
    /// The Database a String data that might be empty, contain metadata, or if it is a
    /// leaf should contain usefull data
    pub data: String,
}

impl Database {
    /// insert_path inserts a given path in the database, where it only creates new nodes
    /// if the do not already exist. It returns the last inserted node so for example a
    /// a value can be inserted ...
    /// # Arguments
    /// * `path` - all subkeys (in correct order) that should be inserted in the database.
    pub fn insert_path(&mut self, path: &[&str]) -> &mut Self {
        let mut node = self;
        for &subkey in path.iter() {
            node = node
                .children
                .get_or_insert(HashMap::new())
                .entry(subkey.to_string())
                .or_insert_with(Database::default);
        }
        node
    }

    /// get_node gets you the node from the database by the exact path. The Function
    /// returns a Result<&Database, String> that either has Ok(node) if the given path
    /// exists or Err("invalid path") if it doesn't
    /// # Arguments
    /// * `path` - all subkeys (in correct order) from the node you want to get.
    pub fn get_node(&self, path: &[&str]) -> Result<&Database, String> {
        let mut node = self;
        //let mut child = self.children;
        for &subkey in path.iter() {
            match &node.children {
                Some(children) => match children.get::<String>(&subkey.to_string()) {
                    Some(value) => node = value,
                    None => return Err(String::from("Invalid path.")),
                },
                None => return Err(String::from("Invalid path.")),
            }
        }
        Ok(node)
    }

    /// get_node_mut works same as get_node but mutual
    /// # Arguments
    /// * `path` - all subkeys (in correct order) from the node you want to get mutually.
    pub fn get_node_mut(&mut self, path: &[&str]) -> Result<&mut Database, String> {
        let mut node = self;
        for &subkey in path.iter() {
            match &mut node.children {
                Some(children) => match children.get_mut::<String>(&subkey.to_string()) {
                    Some(value) => {
                        node = value;
                    }
                    None => {
                        return Err(String::from("Invalid path."));
                    }
                },
                None => return Err(String::from("Invalid path.")),
            }
        }
        Ok(node)
    }

    /// collects all nodes inside this database inside the parent_vec
    /// # Arguments
    /// * `parent_key` - the key of the node you want to get all children key-value pairs from
    /// * `parent_vec` - a mut Vec that all values get collected inside: most probably should be empty when calling the function
    pub fn collect_all_values(&self, parent_key: &str, parent_vec: &mut Vec<(String, String)>) {
        match &self.children {
            Some(children) => {
                for (key, val) in children.iter() {
                    if val.data != "" {
                        parent_vec.push((parent_key.to_owned() + "." + key, val.data.clone()));
                    }
                    val.collect_all_values(
                        &format!("{}.{}", parent_key.to_owned(), key),
                        parent_vec,
                    );
                }
            }
            None => {}
        }
    }

    /// Returns a list of all key-value pairs below the given path if it exists
    /// # Arguments
    /// * `path` - all subkeys (in correct order) from the node you want to get all key-value pairs below it.
    pub fn get_all_values(&mut self, path: &[&str]) -> Result<Vec<(String, String)>, String> {
        match self.get_node(path) {
            Ok(node) => {
                let mut out: std::vec::Vec<(String, String)> = Vec::new();
                node.collect_all_values(&path.join("."), &mut out);
                Ok(out)
            }
            Err(_) => Err("Not implemented!".to_string()),
        }
    }

    /// All children of this database get removed
    /// # Arguments
    /// * `path` - all subkeys (in correct order) from the node you want to clear the children.
    #[allow(dead_code)]
    pub fn clear(&mut self, path: &[&str]) -> Result<(), String> {
        match self.get_node_mut(path) {
            Ok(node) => {
                node.children = None;
                Ok(())
            }
            Err(_) => Err("Invalid delete request".to_string()),
        }
    }

    /// remove the node given by path and key (obviously all children of this node are also lost)
    /// # Arguments
    /// * `path` - all subkeys (in correct order) leading to the hashmap you want to delete a node from.
    /// * `key` - the key of the node you want to delete from the node given by path 
    pub fn delete(&mut self,  path: &[&str], key:&str) -> Result<(), String> {
        match self.get_node_mut(path) {
            Ok(node) => {
                let children = node.children.as_mut();
                match children {
                    Some(x) => {
                        x.remove(key);
                    }
                    None => {
                        return Err("Invalid delete request".to_string());
                    }
                }
            }
            Err(_) => {
                return Err("Invalid delete request".to_string());
            }
        }
        Ok(())
    }
}
/// inform Load-Balancer about existence of this database
/// # Arguments
/// * `lb_addr` - the adress of the load balancer
/// * `db_ip` - the ip adress of this database
/// * `db_port` - the port where this database is listening
pub async fn register_db_with_load_balancer(lb_addr: &str, db_ip: &str, db_port: &str) {
    match TcpStream::connect(lb_addr).await {
        Ok(mut stream) => {
            let register_request = message::get_request_message()
                .with_sender(String::from(db_ip))
                .with_host_address(String::from(db_ip))
                .with_port(String::from(db_port))
                .get_register_db();
            let s = serde_json::to_string(&register_request).unwrap_or_default();
            match stream.write_all(s.as_bytes()).await {
                Ok(_) => {
                    info!(
                        "[DATABASE:] Successfully registered at load-balancer at: {}",
                        lb_addr
                    );
                }
                Err(e) => {
                    error!("[DATABASE:] An error occurred: {}", e);
                }
            };
        }
        Err(_) => {
            warn!("[DATABASE:] Could not connect to load-balancer -> You have to register this DB manually in the load balancer.");
        }
    };
}
