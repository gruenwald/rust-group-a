use serde::{Deserialize, Serialize};
use config::{ConfigError, Config};

/// The default value for the tcp listener address.
const DEFAULT_DB_LISTENER_IP: &str = "0.0.0.0";
/// Returns the default ip the database is listening for incomming requests.
pub fn default_db_listener_ip() -> String {
    String::from(DEFAULT_DB_LISTENER_IP)
}

/// Contains all configuration definitions for the database.
#[derive(Serialize, Deserialize, Debug)]
pub struct DatabaseConfig {
    /// The listener address, where the database listens for incoming requests.
    #[serde(skip, default = "default_db_listener_ip")]
    db_listener_ip: String,
    /// The ip address, under which the database can be reached.
    /// Will be sent to the load-balancer as a callback address.
    db_ip: String,
    /// The configured port, under which the database can be reached.
    /// Will be sent to the load-balancer as a callback address.
    db_port: String,
    /// The ip address, under which the load balancer can be reached.
    /// Is used to send requests to the load-balancer.
    load_balancer_ip: String,
    /// The port, under which the load balancer can be reached.
    /// Is used to send requests to the load-balancer.
    load_balancer_port: String,
}

impl Default for DatabaseConfig {
    fn default() -> Self {
        Self {
            db_listener_ip: DEFAULT_DB_LISTENER_IP.to_string(),
            db_ip: "127.0.0.1".to_string(),
            db_port: "8888".to_string(),
            load_balancer_ip: "127.0.0.1".to_string(),
            load_balancer_port: "8889".to_string(),
        }
    }
}

impl Config<DatabaseConfig> for DatabaseConfig {}

impl DatabaseConfig {
    /// Creates a new configuration from a configuration file.
    /// May result in an error when configuration file could not be found or on any other IO Error.
    /// # Arguments
    /// * `app_name` - Name of configuration file without file-extension.
    /// It will be tried to load the configuratiom from <app_name>.json will automatically appended
    pub fn new(app_name: &str) -> Result<DatabaseConfig, ConfigError> {
        let path = format!("{}{}", app_name, ".json");
        println!("Opening logfile at {}", &path);
        Self::new_from_file(&path)
    }

    /// Returns the configured ip address.
    pub fn db_ip(&self) -> &String {
        &self.db_ip
    }
    /// Returns the configured database port
    pub fn db_port(&self) -> &String {
        &self.db_port
    }
    /// Sets the database port to a new value.
    /// Used for command line arguments to override the file configuration.
    /// # Arguments
    /// * `port` - The new port.
    pub fn set_db_port(&mut self, port: String) {
        self.db_port = port;
    }
    /// Returns the defined address, at which the database listens for incoming connections
    pub fn db_listener_ip(&self) -> &String {
        &self.db_listener_ip
    }
    /// Returns the ip of the load-balancer.
    pub fn load_balancer_ip(&self) -> &String {
        &self.load_balancer_ip
    }
    /// Returns the port of the load-balancer.
    pub fn load_balancer_port(&self) -> &String {
        &self.load_balancer_port
    }
}