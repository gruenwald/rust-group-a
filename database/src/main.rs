/// Database Configuration
mod config;
/// Key-Value database defintion
mod database;

use crate::config::DatabaseConfig;
use database::Database;
use log::*;
use protocol;
use protocol::message;
use std::collections::HashMap;
use std::error::Error;
use std::str;
use tokio;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpListener;

use clap::{App, Arg};

/// Represents the message loop of the database.
/// Accepts incoming requests and responds accordingly
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let cli_matches = App::new("Database Instance")
        .about("Represents a database instance that holds data.")
        .arg(Arg::with_name("port")
            .short("p")
            .long("port")
            .value_name("port")
            .takes_value(true)
        )       
        .help("Defines the port on which the TCP stream listens")
        .get_matches();

    stderrlog::new()
        .module(module_path!())
        .quiet(false)
        .verbosity(6)
        .timestamp(stderrlog::Timestamp::Millisecond)
        .init()
        .unwrap_or_default();
    let mut config: DatabaseConfig = match DatabaseConfig::new(env!("CARGO_PKG_NAME")) {
        Ok(cfg) => cfg,
        Err(e) => {
            println!("An error occurred while parsing the configuration file.");
            println!("Error {}", e);
            println!("Using default config");
            DatabaseConfig::default()
        }
    };
    let port = cli_matches.value_of("port").unwrap_or(&config.db_port()).to_string();
    &config.set_db_port(port);
    
    let db_addr = format!("{}:{}", &config.db_ip(), &config.db_port());
    let db_listener_addr = format!("{}:{}", &config.db_listener_ip(), &config.db_port());
    
    let lb_addr = format!(
        "{}:{}",
        &config.load_balancer_ip(), &config.load_balancer_port()
    );

    info!(
        "[DATABASE:] Trying to register with load-balancer at: {}",
        lb_addr
    );

    database::register_db_with_load_balancer(&lb_addr, &config.db_ip(), &config.db_port()).await;

    // Create the initial Database with a empty HashMap as children. Set the data to the meta
    // information root
    let mut db = Database {
        children: Some(HashMap::new()),
        data: "root".to_string(),
    };

    // Create a new Tcp-listener an give information about the address in the console
    let mut listener = TcpListener::bind(&db_listener_addr).await?;
    info!("[DATABASE:] Listening on: {}", db_listener_addr);

    // start the main loop awaiting a client connections in every iteration. Be aware that this server
    // only excepts on connection at a time
    loop {
        match listener.accept().await {
            Ok((mut socket, _)) => {
                let mut buf = [0; 2048]; // TODO: what happens if buffer not large enough
                loop {
                    // read buffer from socket
                    // awaits a new command that should be executed
                    let n = match socket.read(&mut buf).await {
                        Ok(o) => o,
                        Err(e) => {
                            error!("{}", e);
                            0
                        }
                    };

                    if n == 0 {
                        // client probably closed connection
                        // break the loop so a new client can connect
                        break;
                    }

                    // create a string from the buffer
                    let s = match str::from_utf8(&buf[0..n]) {
                        Ok(v) => v,
                        Err(e) => {
                            error!("{}", e);
                            ""
                        } // TODO: better error handling
                    };

                    // create a command object from the string by deserializing it
                    // Errors give an invalid command that is handled later by just echoing an Error
                    // to the client
                    let response_str: String;

                    match serde_json::from_str::<message::Message>(&s) {
                        Ok(message) => {
                            match &message.msg_type() {
                                // INSERT data into the database
                                message::MessageType::RequestInsert => {
                                    let insert_request =
                                        message::request::deserialize_insert(message.content());

                                    if insert_request.is_valid() {
                                        let subkeys =
                                            insert_request.key().split('.').collect::<Vec<&str>>();
                                        let node = db.insert_path(&subkeys);
                                        node.data = insert_request.value().to_string();

                                        let response = message::get_response_message()
                                            .with_sender(String::from(&db_addr))
                                            .with_status(message::response::Status::Success)
                                            .insert();

                                        response_str =
                                            serde_json::to_string(&response).unwrap_or_default();
                                    } else {
                                        let response = message::get_response_message()
                                            .with_sender(String::from(&db_addr))
                                            .with_status(message::response::Status::Failure)
                                            .insert();

                                        response_str =
                                            serde_json::to_string(&response).unwrap_or_default();
                                    }
                                }
                                // GET data from the databse
                                message::MessageType::RequestGetByFullyQualifiedKey => {
                                    let get_request =
                                        message::request::deserialize_get_by_fully_qualified_key(
                                            message.content(),
                                        );

                                    if get_request.is_valid() {
                                        let subkeys =
                                            get_request.key().split('.').collect::<Vec<&str>>();
                                        match &db.get_node(&subkeys) {
                                            Ok(value) => {
                                                let response = message::get_response_message()
                                                    .with_sender(String::from(&db_addr))
                                                    .with_status(message::response::Status::Success)
                                                    .with_value(value.data.to_string())
                                                    .get_by_fully_qualified_key();
                                                response_str = serde_json::to_string(&response)
                                                    .unwrap_or_default();
                                            }
                                            Err(_) => {
                                                let response = message::get_response_message()
                                                    .with_sender(String::from(&db_addr))
                                                    .with_status(message::response::Status::Failure)
                                                    .with_value(
                                                        "the requested path does not exist"
                                                            .to_string(),
                                                    ) // does this make sense?
                                                    .get_by_fully_qualified_key();
                                                response_str = serde_json::to_string(&response)
                                                    .unwrap_or_default();
                                            }
                                        }
                                    } else {
                                        let response = message::get_response_message()
                                            .with_sender(String::from(&db_addr))
                                            .with_status(message::response::Status::Failure)
                                            .with_value(
                                                "the request content is invalid/not serializeable"
                                                    .to_string(),
                                            ) // does this make sense?
                                            .get_by_fully_qualified_key();
                                        response_str =
                                            serde_json::to_string(&response).unwrap_or_default();
                                    }
                                }
                                // ALL data below a given key is given
                                message::MessageType::RequestGetByPartialKey => {
                                    let all_request =
                                        message::request::deserialize_get_by_partial_key(
                                            message.content(),
                                        );

                                    if all_request.is_valid() {
                                        let subkeys =
                                            all_request.key().split('.').collect::<Vec<&str>>();

                                        match db.get_all_values(&subkeys) {
                                            Ok(value) => {
                                                let response = message::get_response_message()
                                                    .with_sender(String::from(&db_addr))
                                                    .with_status(message::response::Status::Success)
                                                    .with_value(
                                                        serde_json::to_string(&value)
                                                            .unwrap_or_default(),
                                                    )
                                                    .get_by_partial_key();
                                                response_str = serde_json::to_string(&response)
                                                    .unwrap_or_default();
                                            }
                                            Err(e) => {
                                                warn!(
                                                    "An error occurred during get_all_values(): {}",
                                                    e
                                                );
                                                let response = message::get_response_message()
                                                    .with_sender(String::from(&db_addr))
                                                    .with_status(message::response::Status::Failure)
                                                    .with_value(
                                                        "the requested path does not exist"
                                                            .to_string(),
                                                    )
                                                    .get_by_partial_key();
                                                response_str = serde_json::to_string(&response)
                                                    .unwrap_or_default();
                                            }
                                        }
                                    } else {
                                        let response = message::get_response_message()
                                            .with_sender(String::from(&db_addr))
                                            .with_status(message::response::Status::Failure)
                                            .with_value(
                                                "the request content is invalid/not serializeable"
                                                    .to_string(),
                                            )
                                            .get_by_partial_key();
                                        response_str =
                                            serde_json::to_string(&response).unwrap_or_default();
                                    }
                                }
                                // Clear all data below a given key (all data is lost)
                                // If you want to delete just a value do it with the add
                                // command by overiding that overrides the value if a key
                                // already exists
                                message::MessageType::RequestClear => {
                                    let clear_request =
                                        message::request::deserialize_clear(message.content());

                                    if clear_request.is_valid() {
                                        let mut subkeys =
                                            clear_request.key().split('.').collect::<Vec<&str>>();
                                        let last = subkeys.remove(subkeys.len() - 1);

                                        match db.delete(&subkeys, last) {
                                            Ok(_) => {
                                                let response = message::get_response_message()
                                                    .with_sender(String::from(&db_addr))
                                                    .with_status(message::response::Status::Success)
                                                    .clear();
                                                response_str = serde_json::to_string(&response)
                                                    .unwrap_or_default();
                                                    debug!("Deleting subkeys.");
                                            }
                                            Err(e) => {
                                                warn!("An error occurred during clear(): {}", e);
                                                let response = message::get_response_message()
                                                    .with_sender(String::from(&db_addr))
                                                    .with_status(message::response::Status::Failure)
                                                    .clear();
                                                response_str = serde_json::to_string(&response)
                                                    .unwrap_or_default();
                                            }
                                        }
                                    } else {
                                        let response = message::get_response_message()
                                            .with_sender(String::from(&db_addr))
                                            .with_status(message::response::Status::Failure)
                                            .clear();
                                        response_str =
                                            serde_json::to_string(&response).unwrap_or_default();
                                    }
                                }
                                _ => {
                                    // TODO: there is no messag_type error
                                    let response = message::get_response_message()
                                        .with_sender(String::from(&db_addr))
                                        .with_status(message::response::Status::Failure)
                                        .clear();
                                    response_str =
                                        serde_json::to_string(&response).unwrap_or_default();
                                }
                            }
                        }
                        Err(e) => {
                            warn!("An error occurred: {}", e);
                            let response = message::get_response_message()
                                .with_sender(String::from(&db_addr))
                                .with_status(message::response::Status::Failure)
                                .clear();
                            response_str = serde_json::to_string(&response).unwrap_or_default();
                            // todo: msg is invalid
                        }
                    };
                    match socket.write_all(&response_str.as_bytes()).await {
                        Ok(_) => debug!("Response sent: {}", response_str),
                        Err(e) => error!("An error occurred: {}", e),
                    }
                    match socket.shutdown(std::net::Shutdown::Write) {
                        Ok(_) => debug!("Stream shut down."),
                        Err(e) => error!("An error occurred during shutdown: {}", e),
                    };
                }
            }
            Err(e) => {
                // TODO: better error handling
                error!("Error accepting socket; error = {:?}", e);
            }
        }
    }
}
