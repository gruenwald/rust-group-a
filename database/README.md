# The Database Application

## Configuration

The database application relies on an application configuration file named `database.json` next to the executable.

The default configuration:

```json
{
    "db_ip": "127.0.0.1",
    "db_port": "8888",
    "load_balancer_ip" : "127.0.0.1",
    "load_balancer_port": "8889"
}
```

### The `db_ip` Parameter

The public IP of the local machine.

### The `db_port` Parameter

The port the DB listens on.

### The `load_balancer_ip` Parameter

The public IP of the machine the load balancer is running on.

### The `load_balancer_port` Parameter

The port the load balancer listens on.

## How to Use

The Database implements the [protocol](../protocol/readme.md) library. At the moment it accepts the following requests:

- `request::Insert`
- `request::GetByFullyQualifiedKey`
- `request::GetByPartialKey`
- `request::Clear`

## Key Structure

The Database is structured like a tree. Be aware that key and subkey are separated by dots (e.g. `key1.key1_1.key1_1_3`) for `request::Insert` as well as `request::GetByFullyQualifiedKey` and `request::GetByPartialKey`, so the the key itself should not contain any dots.
