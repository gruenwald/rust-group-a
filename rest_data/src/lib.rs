//! # Rest Data - `DataWrapper`
//!
//! A simple Data Wrapper that can be used in conjunction with rocket's `Json<T>` data type.
//! This prevents errors with `String` for production builds.
use serde::Deserialize;
use serde::Serialize;
use std::fmt::Debug;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result;

/// A simple Data Wrapper that can be used in conjunction with rocket's Json<T> data type.
/// This prevents errors with `String` for production builds.
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct DataWrapper {
    #[serde(default)]
    pub(self) body: String,
}

impl DataWrapper {
    /// Create a new `DataWrapper` from a `&str` or `&String`
    pub fn new_from_str(body: &str) -> DataWrapper {
        DataWrapper {
            body: String::from(body),
        }
    }
    /// Create a new `DataWrapper` from a `String`
    pub fn new(body: String) -> DataWrapper {
        DataWrapper { body }
    }
    /// Get the `DataWrapper`'s body as `&str`
    pub fn body_as_str(&self) -> &str {
        self.body.as_str()
    }
    /// Get the `DataWrapper`'s body as `&String` reference.
    pub fn body(&self) -> &String {
        &self.body
    }
}

impl Display for DataWrapper {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "data:{}", self.body)
    }
}
